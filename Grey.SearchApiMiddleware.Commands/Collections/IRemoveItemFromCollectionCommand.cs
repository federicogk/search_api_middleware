﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface IRemoveItemFromCollectionCommand : IAsyncCommand
    {
        IAsyncCommand Configure(string collectionId, string owner, string item);

        IAsyncCommand Configure(string collectionId, string owner, ICollection<string> item);
    }
}
