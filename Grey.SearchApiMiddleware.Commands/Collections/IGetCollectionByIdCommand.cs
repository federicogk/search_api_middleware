﻿using Grey.SearchApiMiddleware.Models;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface IGetCollectionByIdCommand : IAsyncCommand
    {
        Collection Collection { get; }
        IAsyncCommand Configure(string collectionId, string currentUser);
    }
}
