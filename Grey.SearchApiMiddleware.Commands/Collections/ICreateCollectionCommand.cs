﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface ICreateCollectionCommand : IAsyncCommand
    {
        IAsyncCommand Configure(string collectionName, string type, string owner, ICollection<string> items, ICollection<string> shareWith);
    }
}
