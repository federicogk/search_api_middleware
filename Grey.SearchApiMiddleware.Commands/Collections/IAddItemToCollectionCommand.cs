﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface IAddItemToCollectionCommand : IAsyncCommand
    {
        IAsyncCommand Configure(string collectionId, string owner, string itemId);

        IAsyncCommand Configure(string collectionId, string owner, ICollection<string> itemsId);
    }
}
