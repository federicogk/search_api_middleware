﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface IRemoveCollectionCommand : IAsyncCommand
    {
        IAsyncCommand Configure(string collectionId, string loggedUser);
    }
}
