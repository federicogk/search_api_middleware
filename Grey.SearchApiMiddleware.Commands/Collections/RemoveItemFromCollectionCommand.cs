﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class RemoveItemFromCollectionCommand : IRemoveItemFromCollectionCommand
    {
        string _collectionId;
        string _owner;
        ICollection<string> _items;
        ICollectionsRepository Repository { get; set; }

        public RemoveItemFromCollectionCommand(ICollectionsRepository repo)
        {
            Repository = repo;
        }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public IAsyncCommand Configure(string collectionId, string owner, string item)
        {
            _items = new List<string>() { item };
            _owner = owner;
            _collectionId = collectionId;
            return this;
        }

        public IAsyncCommand Configure(string collectionId, string owner, ICollection<string> item)
        {
            _items = item;
            _owner = owner;
            _collectionId = collectionId;
            return this;
        }

        public async Task Execute()
        {
            try
            {
                var collection = Repository.Get(_collectionId);
                if (collection == null)
                {
                    Success = false;
                    Message = string.Format("Collection (id: {0}) not found", _collectionId);
                }
                else
                {
                    if (collection.IsMine(_owner))
                    {
                        foreach (var item in _items)
                        {
                            collection.RemoveItem(item);
                        }
                    }
                    else
                    {
                        Success = false;
                        Message = "Only the owner can remove items to the collection";
                    }
                    
                }
                Success = await Repository.Update(collection);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
        }
    }
}
