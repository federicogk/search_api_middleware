﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class ShareCollectionCommand : IShareCollectionCommand
    {
        string _collectionId;
        ICollection<string> _shareWith;
        ICollectionsRepository Repository { get; set; }

        public ShareCollectionCommand(ICollectionsRepository repo)
        {
            Repository = repo;
        }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public IAsyncCommand Configure(string collectionId, ICollection<string> shareWith)
        {
            _collectionId = collectionId;
            _shareWith = shareWith;
            return this;
        }

        public async Task Execute()
        {
            try
            {
                Collection collection = Repository.Get(_collectionId);
                if (collection != null)
                {
                    collection.Share(_shareWith);

                    Success = await Repository.Update(collection);
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                Success = false;
            }
        }
    }
}
