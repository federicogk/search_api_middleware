﻿using Grey.SearchApiMiddleware.Models;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface IGetCollectionsListCommand : ICommand
    {
        ICollection<ItemDto> List { get; }

        ICommand Configure(string owner);
    }
}
