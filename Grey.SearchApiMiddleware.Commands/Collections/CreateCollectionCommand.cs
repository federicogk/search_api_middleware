﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class CreateCollectionCommand : ICreateCollectionCommand
    {
        string _name;
        string _application;
        string _owner;
        ICollection<string> _items;
        ICollection<string> _shareWith;

        IItemSourceChecker ItemChecker { get; set; }
        ICollectionsRepository Repository { get; set; }

        public CreateCollectionCommand(ICollectionsRepository repository, IItemSourceChecker check)
        {
            Repository = repository;
            ItemChecker = check;
        }

        public bool Success { get; private set; }

        public string Message { get; private set; }

        public IAsyncCommand Configure(string collectionName, string application, string owner, ICollection<string> items, ICollection<string> shareWith)
        {
            _name = collectionName;
            _application = application;
            _owner = owner;
            _items = items;
            _shareWith = shareWith;
            return this;
        }

        public async Task Execute()
        {
            try
            {
                Collection newCollection = new Collection()
                {
                    Id = Guid.NewGuid(),
                    Name = _name,
                    Application = _application,
                    Owner = _owner,
                    ItemIds = _items,
                    SharedWith = _shareWith,
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now
                };

                if (ItemChecker.IsValidApplication(newCollection.Application))
                {
                    if (ItemChecker.CanAdd(newCollection, _items))
                    {
                        if (Repository.IsDuplicated(newCollection))
                        {
                            Message = $"The user {_owner} already have an active Collection of type {_application} called {_name}.";
                        }
                        else
                        {
                            Success = await Repository.CreateCollection((newCollection as BaseCollection));
                            Message = Success ? "Collection created successfully" : "Internal error";

                        }
                    }
                    else
                    {
                        Success = false;
                        Message = "There are Items that does not belongs to the selected application";
                    }
                }
                else
                {
                    Success = false;
                    Message = "This Application is not allowed";
                }
            }
            catch (Exception ex)
            {
                Success = false;
                Message = ex.Message;
            }
        }
    }
}
