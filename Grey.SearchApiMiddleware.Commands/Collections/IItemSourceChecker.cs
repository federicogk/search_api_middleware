﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface IItemSourceChecker
    {
        bool CanAdd(Collection collection, string item);

        bool CanAdd(Collection collection, ICollection<string> items);

        bool IsValidApplication(string application);
    }
}
