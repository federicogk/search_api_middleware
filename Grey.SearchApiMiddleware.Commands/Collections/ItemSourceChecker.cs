﻿using System;
using System.Collections.Generic;
using System.Text;
using Grey.SearchApiMiddleware.Models;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class ItemSourceChecker : IItemSourceChecker
    {
        ICollection<string> _validApps = new List<string>() { "bestof", "people" };

        public bool CanAdd(Collection collection, string item)
        {
            bool canAdd = false;
            switch (collection.Application)
            {
                case "bestof": canAdd = item.ToLower().StartsWith("ggcc_");
                    break;
                case "people":
                    canAdd = item.ToLower().StartsWith("ec");
                    break;
                default:
                    canAdd = false;
                    break;
            }
            return canAdd;
        }

        public bool CanAdd(Collection collection, ICollection<string> items)
        {
            bool canAdd = true;
            foreach (var item in items)
            {
                if (!CanAdd(collection, item))
                {
                    canAdd = false;
                    break;
                }
            }
            return canAdd;
        }

        public bool IsValidApplication(string application)
        {
            return _validApps.Contains(application.ToLowerInvariant());
        }
    }
}
