﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class UpdateCollectionCommand : IUpdateCollectionCommand
    {
        string _collectionId;
        string _newName;
        string _owner;
        ICollectionsRepository Repository { get; set; }

        public UpdateCollectionCommand(ICollectionsRepository repo)
        {
            Repository = repo;
        }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public IAsyncCommand Configure(string collectionId, string owner, string newName)
        {
            _collectionId = collectionId;
            _owner = owner;
            _newName = newName;
            return this;
        }

        public async Task Execute()
        {
            try
            {
                Collection collection = Repository.Get(_collectionId);
                if (collection == null)
                {
                    Success = false;
                    Message = $"The collection {_collectionId} cannot be found";
                }
                else
                {
                    if (collection.IsMine(_owner))
                    {
                        if (Repository.IsDuplicated(collection, _newName))
                        {
                            Success = false;
                            Message = $"The user {_owner} already have an active Collection of type {collection.Application} called {_newName}.";
                        }
                        else
                        {
                            collection.ChangeName(_newName);
                            Success = await Repository.Update(collection);
                            Message = "Collection updated";
                        }
                        
                    }
                    else
                    {
                        Success = false;
                        Message = $"The collection {collection.Name} cannot be updated because you are not the owner";
                    }
                }
            }
            catch (Exception ex)
            {
                Success = false;
                Message = ex.Message;
            }
        }
    }
}
