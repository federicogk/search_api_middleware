﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class RemoveCollectionCommand : IRemoveCollectionCommand
    {
        string _loggedUser;
        string _collectionId;
        ICollectionsRepository Repository { get; set; }
        public RemoveCollectionCommand(ICollectionsRepository repo)
        {
            Repository = repo;
        }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public IAsyncCommand Configure(string collectionId, string loggedUser)
        {
            _loggedUser = loggedUser;
            _collectionId = collectionId;
            return this;
        }

        public async Task Execute()
        {
            try
            {
                Collection collection = Repository.Get(_collectionId);
                bool softDelete = false;
                if (collection == null)
                {
                    Success = false;
                    Message = string.Format("Collection (id: {0}) not found", _collectionId);
                }
                else
                {
                    string currentName = string.Empty;
                    if (collection.IsMine(_loggedUser))
                    {
                        currentName = collection.Name;
                        collection.ChangeName($"deleted.{currentName}");
                        collection.Delete();
                        softDelete = true;
                    }
                    else
                    {
                        collection.Unshare(_loggedUser);
                    }
                    Success = await Repository.Update(collection);
                    if (Success)
                    {
                        Message = softDelete ? string.Format("The Collection {0} was deleted", currentName) 
                            : string.Format("The Collection {0} is no longer shared with {1}", collection.Name, _loggedUser);
                    }
                    else
                    {
                        Message = "There was an error. Changes cannot be saved";
                    }
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
        }
    }
}
