﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class GetCollectionsCommand : IGetCollectionsCommand
    {
        string _owner;
        bool _myCollections;
        bool _sharedCollections;
        
        ICollectionsRepository Repository { get; set; }
        IAzureSearch Search { get; set; }
        public GetCollectionsCommand(ICollectionsRepository repository, IAzureSearch search)
        {
            Repository = repository;
            Search = search;
        }

        public List<Collection> Collections { get; private set; }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public IAsyncCommand All(string owner)
        {
            _owner = owner;
            _myCollections = true;
            _sharedCollections = true;
            return this;
        }
        public IAsyncCommand Mine(string owner)
        {
            _owner = owner;
            _myCollections = true;
            _sharedCollections = false;
            return this;
        }
        public IAsyncCommand Shared(string owner)
        {
            _owner = owner;
            _myCollections = false;
            _sharedCollections = true;
            return this;
        }

        public async Task Execute()
        {
            Collections = new List<Collection>();
            try
            {
                if (_myCollections) { Collections.AddRange(Repository.GetMyCollections(_owner)); }

                if (_sharedCollections) { Collections.AddRange(Repository.GetSharedCollections(_owner)); }

                foreach (var collection in Collections)
                {
                    try
                    {
                        dynamic element = null;
                        switch (collection.Application)
                        {
                            case "bestof":
                                element = await Search.GetGPElementsByID<BestOfItem>(collection.ItemIds);
                                break;
                            case "people":
                                element = await Search.GetGPElementsByID<PeopleItem>(collection.ItemIds);
                                break;
                            case "intelligence":
                                break;
                            case "news":
                                break;
                            default:
                                break;
                        }
                        collection.Items.AddRange(element);
                    }
                    catch (Exception) { }
                }

                Success = Collections.Count > 0;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                Success = false;
            }
        }
    }
}
