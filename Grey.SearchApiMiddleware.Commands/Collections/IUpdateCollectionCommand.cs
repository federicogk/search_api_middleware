﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface IUpdateCollectionCommand : IAsyncCommand
    {
        IAsyncCommand Configure(string collectionId, string owner, string newName);
    }
}
