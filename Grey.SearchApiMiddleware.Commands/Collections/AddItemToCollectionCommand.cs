﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class AddItemToCollectionCommand : IAddItemToCollectionCommand
    {
        string _collectionId;
        string _owner;
        string[] _items;
        ICollection<string> _errors;

        IItemSourceChecker Check { get; set; }
        ICollectionsRepository Repository { get; set; }

        public AddItemToCollectionCommand(ICollectionsRepository repo, IItemSourceChecker check)
        {
            Check = check;
            Repository = repo;
            _errors = new List<string>();
        }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public IAsyncCommand Configure(string collectionId, string owner, string itemId)
        {
            _items = new string[1] { itemId };
            _owner = owner;
            _collectionId = collectionId;
            return this;
        }

        public IAsyncCommand Configure(string collectionId, string owner, ICollection<string> itemsId)
        {
            _items = itemsId.ToArray();
            _owner = owner;
            _collectionId = collectionId;
            return this;
        }

        public async Task Execute()
        {
            try
            {
                var collection = Repository.Get(_collectionId);
                if (collection == null)
                {
                    Success = false;
                    Message = string.Format("Collection (id: {0}) not found", _collectionId);
                }
                else
                {
                    if (collection.IsMine(_owner))
                    {
                        for (int i = 0; i < _items.Length; i++)
                        {
                            if (Check.CanAdd(collection, _items[i]))
                            {
                                collection.AddItem(_items[i]);
                            }
                            else
                            {
                                _errors.Add(_items[i]);
                            }
                        }
                        Success = await Repository.Update(collection);
                        if (_errors.Any()) {
                            Message = String.Format("The Items [{0}] cannot be added because they not belongs to the same application that the collection", string.Join(",", _errors));
                        }
                    }
                    else
                    {
                        Success = false;
                        Message = "Only the owner can add items to the collection";
                    }
                }
            }
            catch (Exception ex)
            {
                Success = false;
                Message = ex.Message;
            }
        }
    }
}
