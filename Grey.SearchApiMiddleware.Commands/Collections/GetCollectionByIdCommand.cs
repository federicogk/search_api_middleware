﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class GetCollectionByIdCommand : IGetCollectionByIdCommand
    {
        string _currentUser;
        string _collectionId;
        IAzureSearch Search { get; set; }
        ICollectionsRepository Repository { get; set; }

        public GetCollectionByIdCommand(ICollectionsRepository repository, IAzureSearch search)
        {
            Repository = repository;
            Search = search;
        }

        public Collection Collection { get; private set; }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public IAsyncCommand Configure(string collectionId, string currentUser)
        {
            _collectionId = collectionId;
            _currentUser = currentUser;
            return this;
        }

        public async Task Execute()
        {
            try
            {
                Collection = Repository.Get(_collectionId);

                if (Collection != null)
                {
                    if (Collection.DoIHaveAccess(_currentUser))
                    {
                        try
                        {
                            dynamic element = null;
                            switch (Collection.Application)
                            {
                                case "bestof":
                                    element = await Search.GetGPElementsByID<BestOfItem>(Collection.ItemIds);
                                    break;
                                case "people":
                                    element = await Search.GetGPElementsByID<PeopleItem>(Collection.ItemIds);
                                    break;
                                case "intelligence":
                                    break;
                                case "news":
                                    break;
                                default:
                                    break;
                            }
                            Collection.Items.AddRange(element);
                        }
                        catch (Exception) { }
                        Success = true;
                    }
                    else
                    {
                        Success = false;
                        Message = $"The user {_currentUser} does not have permissions to see this collection";
                    }
                }
                else
                {
                    Success = false;
                    Message = $"Collection {_collectionId} was not found";
                }
            }
            catch (Exception ex)
            {
                Success = false;
                Message = ex.Message;
            }
        }
    }
}
