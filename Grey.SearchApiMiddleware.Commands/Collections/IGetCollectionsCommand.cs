﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface IGetCollectionsCommand : IAsyncCommand
    {
        List<Collection> Collections { get; }

        IAsyncCommand All(string owner);

        IAsyncCommand Shared(string owner);

        IAsyncCommand Mine(string owner);
    }
}
