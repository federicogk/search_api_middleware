﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public interface IShareCollectionCommand : IAsyncCommand
    {
        IAsyncCommand Configure(string collectionId, ICollection<string> shareWith);
    }
}
