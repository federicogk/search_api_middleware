﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Collections
{
    public class GetCollectionsListCommand : IGetCollectionsListCommand
    {
        string _owner;
        ICollectionsRepository Repository { get; set; }
        public GetCollectionsListCommand(ICollectionsRepository repo)
        {
            Repository = repo;
        }

        public ICollection<ItemDto> List { get; private set; }

        public bool Success { get; private set; }

        public string Message { get; private set; }

        public ICommand Configure(string owner)
        {
            _owner = owner;
            return this;
        }

        public void Execute()
        {
            List = new List<ItemDto>();
            try
            {
                var myCollections = Repository.GetMyCollections(_owner);

                List = myCollections.Select(x => new ItemDto(x.Id, x.Name)).ToList();

                Success = List.Count > 0;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                Success = false;
            } 
        }
    }
}
