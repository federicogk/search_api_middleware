﻿using Grey.SearchApiMiddleware.Models.BestOf;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.BestOf
{
    public interface IBestOfGetPastIssuesCommand : IAsyncCommand
    {
        ICollection<PastIssue> Issues { get; }
    }
}
