﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grey.SearchApiMiddleware.Models.BestOf;

namespace Grey.SearchApiMiddleware.Commands.BestOf
{
    public class BestOfGetPastIssuesCommand : IBestOfGetPastIssuesCommand
    {
        IAzureSearch Search { get; set; }

        public BestOfGetPastIssuesCommand(IAzureSearch search)
        {
            Search = search;
        }

        public ICollection<PastIssue> Issues { get; private set; }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public async Task Execute()
        {
            try
            {
                var results = await Search.SimpleGPSearch<PastIssue>("*", "source eq 'ggcc' and is_bestof eq '1' and bestof_is_archived eq '1'");

                if (results != null)
                {
                    Issues = results.GroupBy(test => test.VolumeId).Select(grp => grp.First()).ToList();
                    Success = true;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                Success = false;
            }
        }
    }
}
