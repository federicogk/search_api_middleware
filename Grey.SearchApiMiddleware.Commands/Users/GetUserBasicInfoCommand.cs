﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Grey.SearchApiMiddleware.Models;

namespace Grey.SearchApiMiddleware.Commands.Users
{
    public class GetUserBasicInfoCommand : IGetUserBasicInfoCommand
    {
        string _email;
        IAzureSearch Search { get; set; }

        public GetUserBasicInfoCommand(IAzureSearch search)
        {
            Search = search;
        }

        public UserBasicInfo Info { get; private set; }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public IAsyncCommand Configure(string email)
        {
            _email = email;
            return this;
        }

        public async Task Execute()
        {
            try
            {
                Info = await Search.GetUserByEmail(_email);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
