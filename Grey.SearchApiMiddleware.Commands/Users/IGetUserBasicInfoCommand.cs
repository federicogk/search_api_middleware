﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.Users
{
    public interface IGetUserBasicInfoCommand : IAsyncCommand
    {
        UserBasicInfo Info { get; }
        IAsyncCommand Configure(string email);
    }
}
