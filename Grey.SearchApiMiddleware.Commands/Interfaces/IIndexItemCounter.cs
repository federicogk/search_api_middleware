﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands
{
    public interface IIndexItemCounter
    {
        Task<AppCount> GetGPCount(string itemName, string filterClause);

        Task<AppCount> GetNewsCount(string itemName, string filterClause);
    }
}
