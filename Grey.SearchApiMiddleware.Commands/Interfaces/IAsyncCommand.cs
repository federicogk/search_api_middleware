﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands
{
    public interface IAsyncCommand
    {
        string Message { get; }

        bool Success { get; }

        Task Execute();
    }
}
