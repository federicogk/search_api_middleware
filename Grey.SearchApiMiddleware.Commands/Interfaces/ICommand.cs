﻿using System;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands
{
    public interface ICommand
    {
        string Message { get; }

        bool Success { get; }

        void Execute();
    }
}
