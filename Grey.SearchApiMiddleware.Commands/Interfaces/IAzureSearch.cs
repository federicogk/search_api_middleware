﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands
{
    public interface IAzureSearch
    {
        Task<ICollection<Filter>> GetFilters(SearchContext context);
        
        Task<T> GetGPElementByID<T>(string id) where T : class;

        Task<ICollection<T>> GetGPElementsByID<T>(ICollection<string> ids) where T : class;

        Task<UserBasicInfo> GetUserByEmail(string email);

        Task<ICollection<T>> SimpleGPSearch<T>(string searchQuery, string searchFilter) where T : class;
    }
}
