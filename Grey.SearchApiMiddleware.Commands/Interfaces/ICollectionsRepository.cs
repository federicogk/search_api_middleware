﻿using Grey.SearchApiMiddleware.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands
{
    public interface ICollectionsRepository
    {
        bool IsDuplicated(Collection collection, string newName);

        bool IsDuplicated(Collection collection);

        Collection Get(string id);

        Task<Collection> Get(string id, string owner);

        Task<bool> Update(Collection collection);

        Task<bool> CreateCollection(BaseCollection collection);

        ICollection<Collection> GetMyCollections(string owner);

        ICollection<Collection> GetSharedCollections(string owner);
    }
}
