﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.AzureSearch
{
    public class CountPerAppCommand : ICountPerAppCommand
    {
        IIndexItemCounter _itemCounter;

        public CountPerAppCommand(IIndexItemCounter itemCounter)
        {
            _itemCounter = itemCounter;
        }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public ICollection<AppCount> Count { get; private set; }

        public async Task Execute()
        {
            try
            {
                Count = new List<AppCount>();

                var intelligence = await _itemCounter.GetGPCount("Intelligence", "source eq 'publish' and showindirectory eq 1");
                var people = await _itemCounter.GetGPCount("People Finder", "source eq 'persona' and showindirectory eq 1");
                var news = await _itemCounter.GetNewsCount("News", "entity_type eq 'article'");
                var bestof = await _itemCounter.GetGPCount("BestOf", "source eq 'ggcc' and is_bestof eq '1' and active eq 1");
                Count.Add(intelligence);
                Count.Add(people);
                Count.Add(news);
                Count.Add(bestof);
            }
            catch (Exception ex)
            {
                Success = false;
                Message = ex.Message;
            }
        }
    }
}
