﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Commands.AzureSearch
{
    public interface ICountPerAppCommand : IAsyncCommand
    {
        ICollection<AppCount> Count { get; }
    }
}
