﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.AzureSearch
{
    public class GetFiltersCommand : IGetFiltersCommand
    {
        IAzureSearch Search { get; set; }
        SearchContext _context;


        public GetFiltersCommand(IAzureSearch search)
        {
            Filters = new List<Filter>();
            Search = search;
        }

        public IEnumerable<Filter> Filters { get; private set; }

        public string Message { get; private set; }

        public bool Success { get; private set; }

        public IAsyncCommand Configure(SearchContext context)
        {
            _context = context;
            return this;
        }

        public async Task Execute()
        {
            try
            {
                Filters = await Search.GetFilters(_context);
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}
