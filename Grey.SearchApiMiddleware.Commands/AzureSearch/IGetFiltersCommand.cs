﻿using Grey.SearchApiMiddleware.Models;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.Commands.AzureSearch
{
    public interface IGetFiltersCommand : IAsyncCommand
    {
        IEnumerable<Filter> Filters { get; }

        IAsyncCommand Configure(SearchContext context);
    }
}
