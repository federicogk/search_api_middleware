﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using az = Microsoft.Azure.Search.Models;

namespace Grey.SearchApiMiddleware.AzureSearch
{
    public class ParametersBuilder : IParametersBuilder
    {
        public ParametersBuilder() { }

        public az.SearchParameters BuildForSearch(SearchContext searchContext)
        {
            az.SearchParameters indexSearchParameters = new az.SearchParameters();

            if (!String.IsNullOrEmpty(searchContext.F))
            {
                indexSearchParameters.Facets = searchContext.GetFacets();
            }

            if (!String.IsNullOrEmpty(searchContext.A))
            {
                indexSearchParameters.Filter = "";
            }

            if (!String.IsNullOrEmpty(searchContext.S))
            {
                string connector = string.IsNullOrEmpty(indexSearchParameters.Filter) ? string.Empty : "and";
                indexSearchParameters.Filter = String.Format("{0} {1} source eq '{2}'", indexSearchParameters.Filter, connector, searchContext.S);
            }

            if (!String.IsNullOrEmpty(searchContext.R))
            {
                indexSearchParameters.Select = searchContext.GetFieldsToRetrieve();
            }

            if (!String.IsNullOrEmpty(searchContext.D))
            {
                indexSearchParameters.SearchFields = searchContext.GetFieldsToSearchInto();
            }

            if (searchContext.T >= 0)
            {
                indexSearchParameters.Top = searchContext.T;
            }

            if (searchContext.K > 0)
            {
                indexSearchParameters.Skip = searchContext.K;
            }

            if (!String.IsNullOrEmpty(searchContext.O))
            {
                indexSearchParameters.OrderBy = searchContext.GetOrderingFields();
            }

            if (!String.IsNullOrEmpty(searchContext.M))
            {
                indexSearchParameters.SearchMode = searchContext.M.Equals("all") ? az.SearchMode.All : az.SearchMode.Any;
            }

            indexSearchParameters.IncludeTotalResultCount = true;

            return indexSearchParameters;
        }

        public az.SearchParameters BuildForFacets(SearchContext searchContext)
        {
            az.SearchParameters indexSearchParameters = new az.SearchParameters();

            if (!String.IsNullOrEmpty(searchContext.F))
            {
                indexSearchParameters.Facets = searchContext.GetFacets();
            }
            if (!String.IsNullOrEmpty(searchContext.S))
            {
                indexSearchParameters.Filter = String.Format("source eq '{0}' and showindirectory eq 1", searchContext.S);
            }

            indexSearchParameters.Top = 0;

            indexSearchParameters.IncludeTotalResultCount = true;

            return indexSearchParameters;
        }

        //public SuggestParameters BuildForSuggestion(SearchContext searchContext)
        //{
        //    SuggestParameters suggestionParameters = new SuggestParameters();


        //    suggestionParameters.UseFuzzyMatching = true;


        //    if (!String.IsNullOrEmpty(searchContext.S))
        //    {
        //        suggestionParameters.Filter = String.Format("source eq '{0}'", searchContext.S);
        //    }

        //    if (!String.IsNullOrEmpty(searchContext.R))
        //    {
        //        suggestionParameters.Select = searchContext.GetFieldsToRetrieve();
        //    }

        //    if (!String.IsNullOrEmpty(searchContext.D))
        //    {
        //        suggestionParameters.SearchFields = searchContext.GetFieldsToSearchInto();
        //    }

        //    if (searchContext.T > 0)
        //    {
        //        suggestionParameters.Top = searchContext.T;
        //    }


        //    return suggestionParameters;
        //}

        //public SuggestParameters BuildForSuggestion()
        //{
        //    SuggestParameters suggestionParameters = new SuggestParameters();

        //    suggestionParameters.Top = 15;

        //    suggestionParameters.UseFuzzyMatching = true;

        //    suggestionParameters.Select = new List<string>() { "title", "categories", "brand", "campaign" };

        //    return suggestionParameters;
        //}
    }
}
