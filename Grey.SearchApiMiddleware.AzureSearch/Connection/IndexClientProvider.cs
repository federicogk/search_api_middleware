﻿using Microsoft.Azure.Search;
using System;

namespace Grey.SearchApiMiddleware.AzureSearch
{
    public class IndexClientProvider : IIndexClientProvider
    {
        //const string adminApiKey = "E27504520442A32FFFE72C6C2973B2D6"; //PROD
        const string adminApiKey = "D571E04D8FF19465B487FFA833BEADA4"; ///QC
        const string searchServiceName = "grey-search-engine-qa";
        const string indexName = "gp-search-qa";

        static SearchServiceClient _client;
        IAzureSearchServiceParameters _config;

        public IndexClientProvider(IAzureSearchServiceParameters config)
        {
            _config = config;
        }

        public ISearchIndexClient GetGPIndex()
        {
            if (_client == null)
            {
                _client = new SearchServiceClient(_config.ServiceName, new SearchCredentials(_config.ServiceKey));
            }

            return _client.Indexes.GetClient(_config.GPIndexName);
        }

        public ISearchIndexClient GetNewsIndex()
        {
            if (_client == null)
            {
                _client = new SearchServiceClient(_config.ServiceName, new SearchCredentials(_config.ServiceKey));
            }

            return _client.Indexes.GetClient(_config.NewsIndexName);
        }
    }
}
