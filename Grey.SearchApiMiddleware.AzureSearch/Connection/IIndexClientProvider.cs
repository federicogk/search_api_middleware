﻿using Microsoft.Azure.Search;

namespace Grey.SearchApiMiddleware.AzureSearch
{
    public interface IIndexClientProvider
    {
        ISearchIndexClient GetGPIndex();
        ISearchIndexClient GetNewsIndex();
    }
}
