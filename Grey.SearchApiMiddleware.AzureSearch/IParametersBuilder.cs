﻿using Grey.SearchApiMiddleware.Models;
using Microsoft.Azure.Search.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.AzureSearch
{
    public interface IParametersBuilder
    {
        SearchParameters BuildForSearch(SearchContext searchContext);

        SearchParameters BuildForFacets(SearchContext searchContext);
    }
}
