﻿using Grey.SearchApiMiddleware.Commands;
using Grey.SearchApiMiddleware.Models;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Grey.SearchApiMiddleware.AzureSearch
{
    public class AzureSearch : IAzureSearch
    {
        IParametersBuilder _paramBuilder;
        IIndexClientProvider _indexProvider;

        public AzureSearch(IParametersBuilder paramBuilder, IIndexClientProvider indexProvider)
        {
            _paramBuilder = paramBuilder;
            _indexProvider = indexProvider;
        }

        public async Task<ICollection<Filter>> GetFilters(SearchContext context)
        {
            ICollection<Filter> filters = new List<Filter>();
            try
            {
                var index = _indexProvider.GetGPIndex();

                var p = _paramBuilder.BuildForFacets(context);

                var results = await index.Documents.SearchAsync(context.Q, p);

                filters = FiltersConverter.Convert(results.Facets);
            }
            catch (System.Exception ex)
            {
                var m = ex.Message;
            }
            return filters;
        }
        
        public async Task<T> GetGPElementByID<T>(string id) where T : class
        {
            var gpIndex = _indexProvider.GetGPIndex();

            return await gpIndex.Documents.GetAsync<T>(id);
        }

        public async Task<ICollection<T>> GetGPElementsByID<T>(ICollection<string> ids) where T : class
        {
            var gpIndex = _indexProvider.GetGPIndex();

            string idsToSearch = string.Format("search.in(id, '{0}')", string.Join(", ", ids));

            var par = new SearchParameters() {Filter = idsToSearch };

            var results = await gpIndex.Documents.SearchAsync<T>("*", par);

            return results.Results.Select(x => x.Document).ToList();
        }

        public async Task<UserBasicInfo> GetUserByEmail(string userEmail)
        {
            var gpIndex = _indexProvider.GetGPIndex();
            var par = new SearchParameters() { Top = 1, SearchFields = new List<string>() { "email" } };

            var documentResult = await gpIndex.Documents.SearchAsync<UserBasicInfo>(userEmail, par);

            return documentResult.Results.Any() ? documentResult.Results.First().Document : new UserBasicInfo() { Email = userEmail };
        }

        public async Task<ICollection<T>> SimpleGPSearch<T>(string searchQuery, string searchFilter) where T : class
        {
            var gpIndex = _indexProvider.GetGPIndex();

            var par = new SearchParameters() { Filter = searchFilter };

            var results = await gpIndex.Documents.SearchAsync<T>(searchQuery, par);

            return results.Results.Select(x => x.Document).ToList();
        }
    }
}
