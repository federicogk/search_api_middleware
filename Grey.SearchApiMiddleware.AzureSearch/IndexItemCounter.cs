﻿using Grey.SearchApiMiddleware.Commands;
using Grey.SearchApiMiddleware.Models;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.AzureSearch
{
    public class IndexItemCounter : IIndexItemCounter
    {
        IIndexClientProvider _indexProvider;

        public IndexItemCounter(IIndexClientProvider indexProvider)
        {
            _indexProvider = indexProvider;
        }

        public async Task<AppCount> GetGPCount(string itemName, string filterClause)
        {
            var gpIndex = _indexProvider.GetGPIndex();

            return await GetCount(itemName, filterClause, gpIndex);
        }

        public async Task<AppCount> GetNewsCount(string itemName, string filterClause)
        {
            var newsIndex = _indexProvider.GetNewsIndex();

            return await GetCount(itemName, filterClause, newsIndex);
        }

        private async Task<AppCount> GetCount(string itemName, string filterClause, ISearchIndexClient client)
        {
            AppCount appCount = new AppCount(itemName);
            try
            {
                var searchResult = await client.Documents.SearchAsync("*", new SearchParameters() { Filter = filterClause, Top = 0, IncludeTotalResultCount = true });

                if (searchResult != null)
                {
                    appCount.Count = searchResult.Count.GetValueOrDefault();
                }
            }
            catch (System.Exception ex)
            {
                var m = ex.Message;
            }

            return appCount;
        }
    }
}
