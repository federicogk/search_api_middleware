﻿using Grey.SearchApiMiddleware.Commands.AzureSearch;
using Grey.SearchApiMiddleware.Models;
using Microsoft.Azure.Search.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Grey.SearchApiMiddleware.AzureSearch
{
    internal class FiltersConverter
    {
        internal static ICollection<Filter> Convert(FacetResults result)
        {
            ICollection<Filter> filters = new List<Filter>();
            foreach (var item in result)
            {
                var f = new Filter(item.Key, item.Value.Select(x => new FilterValue(x.Value.ToString(), x.Count.GetValueOrDefault())));
                filters.Add(f);
            }
            return filters;
        }
    }
}
