﻿namespace Grey.SearchApiMiddleware.AzureSearch
{
    public interface IAzureSearchServiceParameters
    {
        string NewsIndexName { get; set; }

        string GPIndexName { get; set; }

        string ServiceKey { get; set; }

        string ServiceName { get; set; }
    }
}
