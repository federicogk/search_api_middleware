﻿using Grey.SearchApiMiddleware.Commands.Collections;
using Grey.SearchApiMiddleware.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Tests.Collections
{
    [TestClass]
    public class AddItemToCollectionCommandTest
    {
        [TestMethod]
        public void Execute_WhenSingleValidItemIsSubmittedAndLoggedUserIsOwner_MustAddItToCollection()
        {
            //Arrange
            string itemId = "ggcc_0001";
            string loggedUser = "fedrico.gomez@globant.com";
            Guid id = Guid.NewGuid();
            Collection collection = new Collection() { Id = id, Owner = loggedUser };

            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(id.ToString())).Returns(collection);
            repoMock.Setup(x => x.Update(collection)).Returns(Task.Run(() => true));

            Mock<IItemSourceChecker> checkerMock = new Mock<IItemSourceChecker>();
            checkerMock.Setup(x => x.CanAdd(collection, itemId)).Returns(true);

            var command = new AddItemToCollectionCommand(repoMock.Object, checkerMock.Object);

            //Act
            command.Configure(id.ToString(), loggedUser, itemId).Execute();

            //Assert
            Assert.IsTrue(command.Success);
            Assert.AreEqual(1, collection.ItemIds.Count);
            Assert.AreEqual(itemId, collection.ItemIds.First());
            repoMock.Verify(x => x.Get(id.ToString()));
            repoMock.Verify(x => x.Update(collection));
        }

        [TestMethod]
        public void Execute_WhenItemDoesNotBelongsToTheApplicationType_MustNotAddTheItem()
        {
            //Arrange
            string itemId = "EC0001";
            string loggedUser = "fedrico.gomez@globant.com";
            Guid id = Guid.NewGuid();
            Collection collection = new Collection() { Id = id, Owner = loggedUser, Application = "ggcc" };

            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(id.ToString())).Returns(collection);
            repoMock.Setup(x => x.Update(collection)).Returns(Task.Run(() => true));

            Mock<IItemSourceChecker> checkerMock = new Mock<IItemSourceChecker>();
            checkerMock.Setup(x => x.CanAdd(collection, itemId)).Returns(false);

            var command = new AddItemToCollectionCommand(repoMock.Object, checkerMock.Object);

            //Act
            command.Configure(id.ToString(), loggedUser, itemId).Execute();

            //Assert
            Assert.IsTrue(command.Success);
            Assert.AreEqual(0, collection.ItemIds.Count);
            repoMock.Verify(x => x.Get(id.ToString()));
            repoMock.Verify(x => x.Update(collection));
        }

        [TestMethod]
        public void Execute_WhenLoggedUserIsNotTheOwner_MustNotAddTheItem()
        {
            //Arrange
            string itemId = "ggcc_0001";
            string loggedUser = "fedrico.gomez@globant.com";
            Guid id = Guid.NewGuid();
            Collection collection = new Collection() { Id = id, Owner = "wilmer.beltran@globant.com", Application = "ggcc" };

            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(id.ToString())).Returns(collection);
            repoMock.Setup(x => x.Update(collection)).Returns(Task.Run(() => true));

            Mock<IItemSourceChecker> checkerMock = new Mock<IItemSourceChecker>();
            checkerMock.Setup(x => x.CanAdd(collection, itemId)).Returns(true);

            var command = new AddItemToCollectionCommand(repoMock.Object, checkerMock.Object);

            //Act
            command.Configure(id.ToString(), loggedUser, itemId).Execute();

            //Assert
            Assert.IsFalse(command.Success);
            Assert.AreEqual(0, collection.ItemIds.Count);
            Assert.AreEqual("Only the owner can add items to the collection", command.Message);
            repoMock.Verify(x => x.Get(id.ToString()));
        }

        [TestMethod]
        public void Execute_WhenLargeBatchOfItemsAreSubmited_MustGoTroughParallelForeach()
        {
            string[] items = new string[1000];
            //Arrange
            for (int i = 0; i < 1000; i++)
            {
                items[i] = $"ggcc_{i}";
            }
            
            string loggedUser = "fedrico.gomez@globant.com";
            Guid id = Guid.NewGuid();
            Collection collection = new Collection() { Id = id, Owner = loggedUser, Application = "ggcc" };

            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(id.ToString())).Returns(collection);
            repoMock.Setup(x => x.Update(collection)).Returns(Task.Run(() => true));

            Mock<IItemSourceChecker> checkerMock = new Mock<IItemSourceChecker>();
            checkerMock.Setup(x => x.CanAdd(collection, It.IsAny<string>())).Returns(true);

            var command = new AddItemToCollectionCommand(repoMock.Object, checkerMock.Object);

            //Act
            command.Configure(id.ToString(), loggedUser, items).Execute();

            //Assert
            Assert.IsTrue(command.Success);
            Assert.AreEqual(1000, collection.ItemIds.Count);
        }
    }
}
