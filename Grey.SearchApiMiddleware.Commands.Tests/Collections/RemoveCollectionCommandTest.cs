﻿using Grey.SearchApiMiddleware.Commands.Collections;
using Grey.SearchApiMiddleware.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Tests.Collections
{
    [TestClass]
    public class RemoveCollectionCommandTest
    {
        [TestMethod]
        public void Execute_WhenCollectionIsNotFound_MustNotBeExecuted()
        {
            //Arrange
            string collectionId = Guid.NewGuid().ToString();
            string currentUser = "federico.gomez@globant.com";
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId)).Returns<Collection>(null);

            RemoveCollectionCommand command = new RemoveCollectionCommand(repoMock.Object);

            //Act
            command.Configure(collectionId, currentUser).Execute();

            //Assert
            Assert.IsFalse(command.Success);
        }

        [TestMethod]
        public void Execute_WhenLoggedUserIsNotTheOwner_TheUserMustBeRemovedFromSharedList()
        {
            //Arrange
            Guid collectionId = Guid.NewGuid();
            string currentUser = "federico.gomez@globant.com";
            Collection collection = new Collection() { Id = collectionId, Name = "UnitTestCollection" };
            collection.Owner = "wilmer.beltran@globant.com";
            collection.SharedWith.Add(currentUser);

            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId.ToString())).Returns(collection);
            repoMock.Setup(x => x.Update(collection)).Returns(Task.Run(() => true));

            RemoveCollectionCommand command = new RemoveCollectionCommand(repoMock.Object);

            //Act
            command.Configure(collectionId.ToString(), currentUser).Execute();

            //Assert
            Assert.IsTrue(command.Success);
            Assert.IsFalse(collection.IsDeleted);
            Assert.IsFalse(collection.IsSharedWithMe(currentUser));
            Assert.AreEqual($"The Collection {collection.Name} is no longer shared with {currentUser}", command.Message);
            repoMock.Verify(x => x.Get(collectionId.ToString()));
            repoMock.Verify(x => x.Update(collection));
        }

        [TestMethod]
        public void Execute_WhenLoggedUserIsTheOwner_MustAddPrefixToNameAndSoftDeleteTheCollection()
        {
            //Arrange
            Guid collectionId = Guid.NewGuid();
            string currentUser = "federico.gomez@globant.com";
            string collectionName = "UnitTestCollection";
            Collection collection = new Collection() { Id = collectionId, Name = collectionName };
            collection.Owner = currentUser;

            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId.ToString())).Returns(collection);
            repoMock.Setup(x => x.Update(collection)).Returns(Task.Run(() => true));

            RemoveCollectionCommand command = new RemoveCollectionCommand(repoMock.Object);

            //Act
            command.Configure(collectionId.ToString(), currentUser).Execute();

            //Assert
            Assert.IsTrue(command.Success);
            Assert.IsTrue(collection.IsDeleted);
            Assert.AreEqual($"deleted.{collectionName}", collection.Name);
            Assert.AreEqual($"The Collection {collectionName} was deleted", command.Message);
            repoMock.Verify(x => x.Get(collectionId.ToString()));
            repoMock.Verify(x => x.Update(collection));
        }

        [TestMethod]
        public void Execute_WhenRepositoryUpdateFails_MustReturnFalse()
        {
            //Arrange
            Guid collectionId = Guid.NewGuid();
            string currentUser = "federico.gomez@globant.com";
            string collectionName = "UnitTestCollection";
            Collection collection = new Collection() { Id = collectionId, Name = collectionName };
            collection.Owner = currentUser;

            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId.ToString())).Returns(collection);
            repoMock.Setup(x => x.Update(collection)).Returns(Task.Run(() => false));

            RemoveCollectionCommand command = new RemoveCollectionCommand(repoMock.Object);

            //Act
            command.Configure(collectionId.ToString(), currentUser).Execute();

            //Assert
            Assert.IsFalse(command.Success);
            Assert.AreEqual("There was an error. Changes cannot be saved", command.Message);
            repoMock.Verify(x => x.Get(collectionId.ToString()));
            repoMock.Verify(x => x.Update(collection));
        }
    }
}
