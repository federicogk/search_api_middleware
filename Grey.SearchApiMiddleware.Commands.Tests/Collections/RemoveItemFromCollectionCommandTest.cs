﻿using Grey.SearchApiMiddleware.Commands.Collections;
using Grey.SearchApiMiddleware.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Tests.Collections
{
    [TestClass]
    public class RemoveItemFromCollectionCommandTest
    {
        [TestMethod]
        public void Execute_WhenCollectionDoesNotExists_MustDoNothing()
        {
            //Arrange
            string collectionId = Guid.NewGuid().ToString();
            string currentUser = "federico.gomez@globant.com";
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId)).Returns<Collection>(null);

            //Act
            RemoveItemFromCollectionCommand command = new RemoveItemFromCollectionCommand(repoMock.Object);
            command.Configure(collectionId, currentUser, "").Execute();

            //Assert
            Assert.IsFalse(command.Success);
        }

        [TestMethod]
        public void Execute_WhenLoggedUserIsNotTheOwnerOfTheCollection_MustDoNothing()
        {
            //Arrange
            Guid collectionId = Guid.NewGuid();
            string currentUser = "federico.gomez@globant.com";
            Collection collection = new Collection() { Id = collectionId, Owner = "wilmer.beltran@globant.com" };
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId.ToString())).Returns(collection);

            //Act
            RemoveItemFromCollectionCommand command = new RemoveItemFromCollectionCommand(repoMock.Object);
            command.Configure(collectionId.ToString(), currentUser, "").Execute();

            //Assert
            Assert.IsFalse(command.Success);
            Assert.AreEqual("Only the owner can remove items to the collection", command.Message);
        }

        [TestMethod]
        public void Execute_WhenLoggedUserITheOwnerOfTheCollection_MustRemoveTheItem()
        {
            //Arrange
            Guid collectionId = Guid.NewGuid();
            string currentUser = "federico.gomez@globant.com";
            Collection collection = new Collection() { Id = collectionId, Owner = currentUser };
            collection.ItemIds.Add("ggcc_0001");

            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId.ToString())).Returns(collection);
            repoMock.Setup(x => x.Update(collection)).Returns(Task.Run(() => true));

            //Act
            RemoveItemFromCollectionCommand command = new RemoveItemFromCollectionCommand(repoMock.Object);
            command.Configure(collectionId.ToString(), currentUser, "ggcc_0001").Execute();

            //Assert
            Assert.IsTrue(command.Success);
            Assert.AreEqual(0, collection.ItemIds.Count);
        }
    }
}
