﻿using Grey.SearchApiMiddleware.Commands.Collections;
using Grey.SearchApiMiddleware.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Tests.Collections
{
    [TestClass]
    public class UpdateCollectionCommandTest
    {
        [TestMethod]
        public void Execute_WhenOTheCollectionIsNotFound_MustNotBeExecuted()
        {
            string currentUser = "wilmer.beltran.gomez@globant.com";
            string newName = "theNewName";
            string collectionId = Guid.NewGuid().ToString();

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId)).Returns<Collection>(null);

            //Act
            UpdateCollectionCommand command = new UpdateCollectionCommand(repoMock.Object);
            command.Configure(collectionId, currentUser, newName).Execute();

            //Assert
            Assert.IsFalse(command.Success);
        }

        [TestMethod]
        public void Execute_WhenTheUserIsNotTheOwner_MustNotBeExecuted()
        {
            string newName = "theNewName";
            string collectionId = Guid.NewGuid().ToString();

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            Collection theCollection = new Collection() { Name = "test", Owner = "federico.gomez@globant.com" };
            repoMock.Setup(x => x.Get(collectionId)).Returns(theCollection);
            repoMock.Setup(x => x.IsDuplicated(theCollection, newName)).Returns(true);

            //Act
            UpdateCollectionCommand command = new UpdateCollectionCommand(repoMock.Object);
            command.Configure(collectionId, "wilmer.beltran@globant.com", newName).Execute();

            //Assert
            Assert.IsFalse(command.Success);
            Assert.AreEqual("test", theCollection.Name);
        }

        [TestMethod]
        public void Execute_WhenOtherCollectionExistsWithSameNameOwnerAndApplication_MustNotBeExecuted()
        {
            string currentUser = "wilmer.beltran.gomez@globant.com";
            string newName = "theNewName";
            string collectionId = Guid.NewGuid().ToString();

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            Collection theCollection = new Collection() { Name = "test", Owner = "federico.gomez@globant.com" };
            repoMock.Setup(x => x.Get(collectionId)).Returns(theCollection);
            repoMock.Setup(x => x.IsDuplicated(theCollection, newName)).Returns(true);

            //Act
            UpdateCollectionCommand command = new UpdateCollectionCommand(repoMock.Object);
            command.Configure(collectionId, currentUser, newName).Execute();

            //Assert
            Assert.IsFalse(command.Success);
            Assert.AreEqual("test", theCollection.Name);
        }

        [TestMethod]
        public void Execute_WhenLoggedUserIsOwnerAndNameIsNotDuplicated_MustChangeTheName()
        {
            string currentUser = "federico.gomez@globant.com";
            string newName = "theNewName";
            string collectionId = Guid.NewGuid().ToString();

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            Collection theCollection = new Collection() { Name = "test", Owner = "federico.gomez@globant.com" };
            repoMock.Setup(x => x.Get(collectionId)).Returns(theCollection);
            repoMock.Setup(x => x.IsDuplicated(theCollection, newName)).Returns(false);
            repoMock.Setup(x => x.Update(theCollection)).Returns(Task.Run(() => true));

            //Act
            UpdateCollectionCommand command = new UpdateCollectionCommand(repoMock.Object);
            command.Configure(collectionId, currentUser, newName).Execute();

            //Assert
            Assert.IsTrue(command.Success);
            Assert.AreEqual(newName, theCollection.Name);
        }
    }
}
