﻿using Grey.SearchApiMiddleware.Commands.Collections;
using Grey.SearchApiMiddleware.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.Commands.Tests.Collections
{
    [TestClass]
    public class IItemSourceCheckerTests
    {
        [TestMethod]
        public void CanAdd_WhenAppIsPeopleAndItemIsFromPeople_MustReturnTrue()
        {
            //Arrange
            Collection collection = new Collection() { Application = "people" };
            string item = "EC0001";

            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool canAdd = checker.CanAdd(collection, item);

            Assert.IsTrue(canAdd);
        }

        [TestMethod]
        public void CanAdd_WhenAppIsBestOfAndItemIsFromBestOf_MustReturnTrue()
        {
            //Arrange
            Collection collection = new Collection() { Application = "bestof" };
            string item = "GGCC_001";

            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool canAdd = checker.CanAdd(collection, item);

            Assert.IsTrue(canAdd);
        }

        [TestMethod]
        public void CanAdd_WhenAppIsBestOfAndItemIsNotFromBestOf_MustReturnFalse()
        {
            //Arrange
            Collection collection = new Collection() { Application = "bestof" };
            string item = "article_01";

            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool canAdd = checker.CanAdd(collection, item);

            Assert.IsFalse(canAdd);
        }

        [TestMethod]
        public void CanAdd_WhenAppIsPeopleOfAndItemIsNotFromPeople_MustReturnFalse()
        {
            //Arrange
            Collection collection = new Collection() { Application = "people" };
            string item = "article_01";

            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool canAdd = checker.CanAdd(collection, item);

            Assert.IsFalse(canAdd);
        }

        [TestMethod]
        public void CanAdd_WhenAppNotRegistered_MustReturnFalse()
        {
            //Arrange
            Collection collection = new Collection() { Application = "fakeApp" };
            string item = "article_01";

            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool canAdd = checker.CanAdd(collection, item);

            Assert.IsFalse(canAdd);
        }



        [TestMethod]
        public void CanAdd_WhenAppIsPeopleAndAllItemsAreFromPeople_MustReturnTrue()
        {
            //Arrange
            Collection collection = new Collection() { Application = "people" };
            ICollection<string> items = new List<string>() { "EC0002", "EC0003", "EC0004" };

            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool canAdd = checker.CanAdd(collection, items);

            Assert.IsTrue(canAdd);
        }

        [TestMethod]
        public void CanAdd_WhenAppIsBestOfAndAllItemsAreFromBestOf_MustReturnTrue()
        {
            //Arrange
            Collection collection = new Collection() { Application = "bestof" };
            ICollection<string> items = new List<string>() { "GGCC_001", "GGCC_002", "GGCC_003" };

            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool canAdd = checker.CanAdd(collection, items);

            Assert.IsTrue(canAdd);
        }

        [TestMethod]
        public void CanAdd_WhenAppIsPeopleButSomeItemsAreNotFromPeople_MustReturnFalse()
        {
            //Arrange
            Collection collection = new Collection() { Application = "people" };
            ICollection<string> items = new List<string>() { "EC0002", "GGCC_001", "EC0004" };

            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool canAdd = checker.CanAdd(collection, items);

            Assert.IsFalse(canAdd);
        }

        [TestMethod]
        public void CanAdd_WhenAppIsBestOfButSomeItemsAreNotFromBestOf_MustReturnFalse()
        {
            //Arrange
            Collection collection = new Collection() { Application = "bestof" };
            ICollection<string> items = new List<string>() { "GGCC_001", "Article_003", "GGCC_003" };

            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool canAdd = checker.CanAdd(collection, items);

            Assert.IsFalse(canAdd);
        }



        [TestMethod]
        public void IsValidApp_WhenAppIsPeople_MustReturnTrue()
        {
            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool isValid = checker.IsValidApplication("people");

            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void IsValidApp_WhenAppIsBestOf_MustReturnTrue()
        {
            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool isValid = checker.IsValidApplication("bestof");

            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void IsValidApp_WhenAppSINotInList_MustReturnFalse()
        {
            //Act
            ItemSourceChecker checker = new ItemSourceChecker();
            bool isValid = checker.IsValidApplication("sharepoint");

            Assert.IsFalse(isValid);
        }

    }
}
