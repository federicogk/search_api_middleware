﻿using Grey.SearchApiMiddleware.Commands.Collections;
using Grey.SearchApiMiddleware.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Tests.Collections
{
    [TestClass]
    public class ShareCollectionCommandTest
    {
        [TestMethod]
        public void Execute_WhenCollectionDoesNotExists_MustDoNothing()
        {
            string collectionId = Guid.NewGuid().ToString();

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId)).Returns<Collection>(null);

            //Act
            ShareCollectionCommand command = new ShareCollectionCommand(repoMock.Object);
            command.Configure(collectionId, new List<string>()).Execute();

            //Assert
            Assert.IsFalse(command.Success);
        }

        [TestMethod]
        public void Execute_WhenCollectionExistsAndEmailsAreAvalid_MustAddTheUsers()
        {
            Guid collectionId = Guid.NewGuid();
            IList<string> users = new List<string>() { "federico.gomez@globant.com", "wilmer.beltran@globant.com" };
            Collection collection = new Collection() { Id = collectionId };

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.Get(collectionId.ToString())).Returns(collection);
            repoMock.Setup(x => x.Update(collection)).Returns(Task.Run(() => true));

            //Act
            ShareCollectionCommand command = new ShareCollectionCommand(repoMock.Object);
            command.Configure(collectionId.ToString(), users).Execute();

            //Assert
            Assert.IsTrue(command.Success);
            Assert.AreEqual(2, collection.SharedWith.Count);
        }
    }
}
