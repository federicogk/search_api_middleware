﻿using Grey.SearchApiMiddleware.Commands.Collections;
using Grey.SearchApiMiddleware.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.Commands.Tests.Collections
{
    [TestClass]
    public class CreateCollectionCommandTests
    {
        [TestMethod]
        public void Execute_WhenTheApplicationIsNotValid_MustAvoidExecution()
        {
            string owner = "federico.gomez@globant.com";
            string name = "test collection";
            string application = "notvalidapp";

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.IsDuplicated(It.IsAny<Collection>())).Returns(true);

            Mock<IItemSourceChecker> checkerMock = new Mock<IItemSourceChecker>();
            checkerMock.Setup(x => x.IsValidApplication(It.IsAny<string>())).Returns(false);

            //Act
            CreateCollectionCommand command = new CreateCollectionCommand(repoMock.Object, checkerMock.Object);
            command.Configure(name, application, owner, new List<string>(), new List<string>()).Execute();

            //Assert
            Assert.IsFalse(command.Success);
            Assert.AreEqual("This Application is not allowed", command.Message);
            repoMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void Execute_WhenTheApplicationIsValidButThereAreInitialItemsThatDoesNotBelongsToApp_MustAvoidExecution()
        {
            string owner = "federico.gomez@globant.com";
            string name = "test collection";
            string application = "people";
            ICollection<string> items = new List<string>();

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.IsDuplicated(It.IsAny<Collection>())).Returns(true);

            Mock<IItemSourceChecker> checkerMock = new Mock<IItemSourceChecker>();
            checkerMock.Setup(x => x.IsValidApplication(It.IsAny<string>())).Returns(true);
            checkerMock.Setup(x => x.CanAdd(new Collection(), items)).Returns(false);

            //Act
            CreateCollectionCommand command = new CreateCollectionCommand(repoMock.Object, checkerMock.Object);
            command.Configure(name, application, owner, new List<string>(), new List<string>()).Execute();

            //Assert
            Assert.IsFalse(command.Success);
            Assert.AreEqual("There are Items that does not belongs to the selected application", command.Message);
            repoMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void Execute_WhenTheUserAlreadyHaveACollectionWithSameNameAndApplication_MustAvoidExecution()
        {
            string owner = "federico.gomez@globant.com";
            string name = "test collection";
            string application = "people";
            ICollection<string> items = new List<string>();

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.IsDuplicated(It.IsAny<Collection>())).Returns(true);

            Mock<IItemSourceChecker> checkerMock = new Mock<IItemSourceChecker>();
            checkerMock.Setup(x => x.IsValidApplication(It.IsAny<string>())).Returns(true);
            checkerMock.Setup(x => x.CanAdd(It.IsAny<Collection>(), items)).Returns(true);

            //Act
            CreateCollectionCommand command = new CreateCollectionCommand(repoMock.Object, checkerMock.Object);
            command.Configure(name, application, owner, new List<string>(), new List<string>()).Execute();

            //Assert
            Assert.IsFalse(command.Success);
            repoMock.Verify(x => x.IsDuplicated(It.IsAny<Collection>()));
            repoMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void Execute_WhenAllValidationPassesAndCollectionIsSaved_MustReturnSuccessfuly()
        {
            string owner = "federico.gomez@globant.com";
            string name = "test collection";
            string application = "people";
            ICollection<string> items = new List<string>();

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.IsDuplicated(It.IsAny<Collection>())).Returns(false);
            repoMock.Setup(x => x.CreateCollection(It.IsAny<Collection>())).ReturnsAsync(()=>true);

            Mock<IItemSourceChecker> checkerMock = new Mock<IItemSourceChecker>();
            checkerMock.Setup(x => x.IsValidApplication(It.IsAny<string>())).Returns(true);
            checkerMock.Setup(x => x.CanAdd(It.IsAny<Collection>(), items)).Returns(true);

            //Act
            CreateCollectionCommand command = new CreateCollectionCommand(repoMock.Object, checkerMock.Object);
            command.Configure(name, application, owner, new List<string>(), new List<string>()).Execute();

            //Assert
            Assert.IsTrue(command.Success);
            Assert.AreEqual("Collection created successfully", command.Message);
            repoMock.Verify(x => x.IsDuplicated(It.IsAny<Collection>()));
            repoMock.Verify(x => x.CreateCollection(It.IsAny<Collection>()));
            repoMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void Execute_WhenAllValidationPassesButCreationFails_MustReturnError()
        {
            string owner = "federico.gomez@globant.com";
            string name = "test collection";
            string application = "people";
            ICollection<string> items = new List<string>();

            //Arrange
            Mock<ICollectionsRepository> repoMock = new Mock<ICollectionsRepository>();
            repoMock.Setup(x => x.IsDuplicated(It.IsAny<Collection>())).Returns(false);
            repoMock.Setup(x => x.CreateCollection(It.IsAny<Collection>())).ReturnsAsync(() => false);

            Mock<IItemSourceChecker> checkerMock = new Mock<IItemSourceChecker>();
            checkerMock.Setup(x => x.IsValidApplication(It.IsAny<string>())).Returns(true);
            checkerMock.Setup(x => x.CanAdd(It.IsAny<Collection>(), items)).Returns(true);

            //Act
            CreateCollectionCommand command = new CreateCollectionCommand(repoMock.Object, checkerMock.Object);
            command.Configure(name, application, owner, new List<string>(), new List<string>()).Execute();

            //Assert
            Assert.IsFalse(command.Success);
            Assert.AreEqual("Internal error", command.Message);
            repoMock.Verify(x => x.IsDuplicated(It.IsAny<Collection>()));
            repoMock.Verify(x => x.CreateCollection(It.IsAny<Collection>()));
            repoMock.VerifyNoOtherCalls();
        }
    }
}
