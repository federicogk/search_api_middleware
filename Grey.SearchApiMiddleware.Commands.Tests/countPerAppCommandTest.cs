using Grey.SearchApiMiddleware.Commands.AzureSearch;
using Grey.SearchApiMiddleware.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Commands.Tests
{
    [TestClass]
    public class CountPerAppCommandTest
    {
        [TestMethod]
        public void Execute_WhenSuccess_MustReturnOrderedItems()
        {
            AppCount appIn = new AppCount("Intelligence", 10);
            AppCount appPF= new AppCount("People Finder", 23);
            AppCount appBes = new AppCount("BestOf", 12);
            AppCount appNews = new AppCount("News", 9);

            Mock<IIndexItemCounter> counterMock = new Mock<IIndexItemCounter>();
            counterMock.Setup(x => x.GetGPCount("Intelligence", It.IsAny<string>())).Returns(Task.FromResult(appIn));
            counterMock.Setup(x => x.GetGPCount("People Finder", It.IsAny<string>())).Returns(Task.FromResult(appPF));
            counterMock.Setup(x => x.GetGPCount("BestOf", It.IsAny<string>())).Returns(Task.FromResult(appBes));
            counterMock.Setup(x => x.GetNewsCount("News", It.IsAny<string>())).Returns(Task.FromResult(appNews));

            CountPerAppCommand command = new CountPerAppCommand(counterMock.Object);
            command.Execute();

            Assert.AreEqual(4, command.Count.Count);
            Assert.AreEqual("Intelligence", command.Count.ElementAt(0).Name);
            Assert.AreEqual("People Finder", command.Count.ElementAt(1).Name);
            Assert.AreEqual("News", command.Count.ElementAt(2).Name);
            Assert.AreEqual("BestOf", command.Count.ElementAt(3).Name);
        }
    }
}
