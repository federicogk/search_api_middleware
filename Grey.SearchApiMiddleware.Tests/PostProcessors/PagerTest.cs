﻿using Grey.SearchApiMiddleware.Models;
using Grey.SearchApiMiddleware.PostProcessors;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Tests.PostProcessors
{
    [TestClass]
    public class PagerTest
    {
        [TestMethod]
        public void Pager_DoPage_WhenResultsAreEmpty_MustReturnEnEmptyCollection()
        {
            var parametersMock = new Mock<IApiParameters>();
            parametersMock.Setup(x => x.MaxCnt).Returns(5);
            
            Pager pager = new Pager(parametersMock.Object);

            var paginatedResults = pager.PageGroups(new List<SearchGroup>());

            Assert.IsNotNull(paginatedResults);
            Assert.AreEqual(paginatedResults.Count, 0);
        }
    }
}
