﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Grey.SearchApiMiddleware.Models;

namespace Grey.SearchApiMiddleware.Tests.Model
{
    [TestClass]
    public class GroupingConfigurationManagerTest
    {
        [TestMethod]
        public void GroupingConfigurationManager_GetForSource_WhenExists_MustReturnTheFirstThatMatches()
        {
            //Arrange
            ICollection<SourceElement> sources = new List<SourceElement>() { new SourceElement() { Source = "Persona" } };

            var configurations = new List<GroupingConfiguration>();
            configurations.Add(new GroupingConfiguration() { Name = "Config-A",  Sources = sources });
            configurations.Add(new GroupingConfiguration() { Name = "Config-B", Sources = sources });

            var parametersMock = new Mock<IApiParameters>();
            parametersMock.Setup(x => x.GetGroupingConfiguration()).Returns(configurations);

            var searchItem = new SearchItem() { source = "Persona" };
            
            //Act
            var configManager = new GroupingConfigurationManager(parametersMock.Object);
            var config = configManager.GetForSource(searchItem);

            //Assert
            Assert.IsNotNull(config);
            Assert.AreEqual(config.Name, "Config-A");
        }

        [TestMethod]
        public void GroupingConfigurationManager_GetForSource_WhenNotExists_MustReturnNullObject()
        {
            //Arrange
            var configurations = new List<GroupingConfiguration>();

            var parametersMock = new Mock<IApiParameters>();
            parametersMock.Setup(x => x.GetGroupingConfiguration()).Returns(configurations);

            var searchItem = new SearchItem() { source = "Persona" };

            //Act
            var configManager = new GroupingConfigurationManager(parametersMock.Object);
            var config = configManager.GetForSource(searchItem);

            //Assert
            Assert.IsNotNull(config);
            Assert.AreEqual(config.Name, "Unknown");
            Assert.AreEqual(config.GroupCount, 100);
        }
    }
}
