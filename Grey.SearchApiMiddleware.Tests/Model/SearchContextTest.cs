﻿//using Grey.SearchApiMiddleware.Models;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Grey.SearchApiMiddleware.Tests.Model
//{
//    [TestClass]
//    public class SearchContextTest
//    {
//        [TestMethod]
//        public void SearchContextParams_GetKey_WhenCalled_MustReturnATupleWithAllTheKeys()
//        {
//            //Arrange
//            SearchContext contextParams = new SearchContext();
//            contextParams.Q = "federico.gomez";
//            contextParams.S = "persona";
//            contextParams.C = "false";
//            contextParams.Client = "GPSearch";

//            //Act
//            var key = contextParams.GetSearchKey();

//            //Assert
//            Assert.AreEqual(key.Item1, "federico.gomez");
//            Assert.AreEqual(key.Item2, "persona");
//            Assert.AreEqual(key.Item3, "false");
//            Assert.AreEqual(key.Item4, "GPSearch");
//        }

//        [TestMethod]
//        public void SearchContextParams_GetKey_WhenParameterIsNotPresent_MustReturnEmptryString()
//        {
//            //Arrange
//            SearchContext contextParams = new SearchContext();
//            contextParams.Q = "federico.gomez";
//            contextParams.C = "false";

//            //Act
//            var key = contextParams.GetSearchKey();

//            //Assert
//            Assert.AreEqual(key.Item1, "federico.gomez");
//            Assert.AreEqual(key.Item2, string.Empty);
//            Assert.AreEqual(key.Item3, "false");
//        }

//        [TestMethod]
//        public void SearchContextParams_GetQueryString_WithOneParameter_MustAddQuestionMark()
//        {
//            //Arrange
//            SearchContext contextParams = new SearchContext();
//            contextParams.Q = "federico.gomez";

//            //Act
//            string queryString = contextParams.GetQueryString();

//            //Assert
//            Assert.AreEqual(queryString, "?q=federico.gomez");
//        }

//        [TestMethod]
//        public void SearchContextParams_GetQueryString_WithTwoParameter_MustAddQuestionMarkAndAmpersand()
//        {
//            //Arrange
//            SearchContext contextParams = new SearchContext();
//            contextParams.Q = "federico.gomez";
//            contextParams.S = "persona";

//            //Act
//            string queryString = contextParams.GetQueryString();

//            //Assert
//            Assert.AreEqual(queryString, "?q=federico.gomez&s=persona");
//        }

//        [TestMethod]
//        public void SearchContextParams_GetQueryString_WithPParameter_MustAddHisValueIfTrue()
//        {
//            //Arrange
//            SearchContext contextParams = new SearchContext();
//            contextParams.Q = "federico.gomez";
//            contextParams.S = "persona";
//            contextParams.P = true;

//            //Act
//            string queryString = contextParams.GetQueryString();

//            //Assert
//            Assert.AreEqual(queryString, "?q=federico.gomez&s=persona&p=True");
//        }

//        [TestMethod]
//        public void SearchContextParams_GetQueryString_WithAllParameters_MustReturnFullQueryStringSkippingPageAndCacheParams()
//        {
//            //Arrange
//            SearchContext contextParams = new SearchContext();
//            contextParams.Q = "federico.gomez";
//            contextParams.F = "EMEA,LATAM";
//            contextParams.S = "persona";
//            contextParams.P = true;
//            contextParams.M = "ALL";
//            contextParams.O = "email";
//            contextParams.R = "title,email,givenname";
//            contextParams.D = "title,email";
//            contextParams.T = 40;
//            contextParams.K = 15;
//            //contextParams.U = "uri";
//            //contextParams.V = "2.0.0";
//            contextParams.C = "f";
//            contextParams.Page = "2";
//            contextParams.CacheAllPages = "true";

//            //Act
//            string queryString = contextParams.GetQueryString();

//            //Assert
//            Assert.IsTrue(queryString.StartsWith("?"));
//            Assert.IsTrue(queryString.Contains("q=federico.gomez"));
//            Assert.IsTrue(queryString.Contains("&f=EMEA,LATAM"));
//            Assert.IsTrue(queryString.Contains("&s=persona"));
//            Assert.IsTrue(queryString.Contains("&p=True"));
//            Assert.IsTrue(queryString.Contains("&m=ALL"));
//            Assert.IsTrue(queryString.Contains("&o=email"));
//            Assert.IsTrue(queryString.Contains("&r=title,email,givenname"));
//            Assert.IsTrue(queryString.Contains("&d=title,email"));
//            Assert.IsTrue(queryString.Contains("&t=40"));
//            Assert.IsTrue(queryString.Contains("&k=15"));
//            //Assert.IsTrue(queryString.Contains("&u=uri"));
//            //Assert.IsTrue(queryString.Contains("&v=2.0.0"));
//            Assert.IsTrue(queryString.Contains("&c=f"));
//            Assert.IsFalse(queryString.Contains("&page"));
//            Assert.IsFalse(queryString.Contains("&CacheAllPages"));
//        }
//    }
//}

