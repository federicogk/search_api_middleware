﻿using Grey.SearchApiMiddleware.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Tests.Model
{
    [TestClass]
    public class GroupsContainerTest
    {
        [TestMethod]
        public void GroupsContainer_ExistsGroup_WhenGroupDoesNotExists_MustReturnFalse()
        {
            var container = new GroupsContainer();

            Assert.IsFalse(container.ExistsGroup("test"));
        }

        [TestMethod]
        public void GroupsContainer_ExistsGroup_WhenGroupExists_MustReturnTrue()
        {
            var container = new GroupsContainer();
            container.CreateGroup("test");
            Assert.IsTrue(container.ExistsGroup("test"));
        }

        [TestMethod]
        public void GroupsContainer_CreateGroup_MustAddANewGroupToTheCollection()
        {
            var container = new GroupsContainer();
            Assert.IsFalse(container.ExistsGroup("test"));

            container.CreateGroup("test");
            Assert.IsTrue(container.ExistsGroup("test"));
        }

        [TestMethod]
        public void GroupsContainer_CreateGroupWithItem_MustAddANewGroupToTheCollectionWithHisItem()
        {
            var container = new GroupsContainer();
            Assert.IsFalse(container.ExistsGroup("test"));

            container.CreateGroup("test", new SearchItem());

            Assert.IsTrue(container.ExistsGroup("test"));
            Assert.AreEqual(container.GetItemsCount("test"), 1);
        }

        [TestMethod]
        public void GroupsContainer_AddToGroup_MustAddTheItemToTheGroupIfExists()
        {
            var container = new GroupsContainer();
            
            container.CreateGroup("test");
            container.AddToGroup("test", new SearchItem());
            container.AddToGroup("test", new SearchItem());
            
            Assert.AreEqual(container.GetItemsCount("test"), 2);
        }
    }
}
