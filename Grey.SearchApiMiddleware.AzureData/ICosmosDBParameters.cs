﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.AzureData
{
    public interface ICosmosDBParameters
    {
        string CollectionsDBEndpoint { get; set; }
        string CollectionsDBEndpointKey { get; set; }
        string CollectionsDBDatabaseName { get; set; }
        string CollectionsDBCollectionName { get; set; }
    }
}
