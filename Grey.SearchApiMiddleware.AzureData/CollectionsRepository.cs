﻿using Grey.SearchApiMiddleware.Commands;
using Grey.SearchApiMiddleware.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.AzureData
{
    public class CollectionsRepository : ICollectionsRepository
    {
        IDocumentClient _client;
        ICosmosDBParameters _parameters;

        public CollectionsRepository(IDocumentClient client, ICosmosDBParameters parameters)
        {
            _client = client;
            _parameters = parameters;
        }

        public bool IsDuplicated(Collection collection, string newName)
        {
            return IsDuplicated(collection.Owner, collection.Application, newName);
        }

        public bool IsDuplicated(Collection collection)
        {
            return IsDuplicated(collection.Owner, collection.Application, collection.Name);
        }

        private bool IsDuplicated(string owner, string application, string name)
        {
            bool isDuplicated = false;
            try
            {
                var uri = UriFactory.CreateDocumentCollectionUri(_parameters.CollectionsDBDatabaseName, _parameters.CollectionsDBCollectionName);

                var collections = _client.CreateDocumentQuery<Collection>(uri)
                                           .Where(x => x.Owner.Equals(owner) &&
                                                       x.Application.Equals(application) &&
                                                       x.Name.Equals(name) &&
                                                       x.IsDeleted == false)
                                           .AsEnumerable()
                                           .FirstOrDefault();

                isDuplicated = collections != null;
            }
            catch (Exception ex)
            {

            }

            return isDuplicated;
        }

        public async Task<Collection> Get(string id, string owner)
        {
            Collection collection = null;
            try
            {
                var uri = UriFactory.CreateDocumentUri(_parameters.CollectionsDBDatabaseName, _parameters.CollectionsDBCollectionName, id);

                collection = await _client.ReadDocumentAsync<Collection>(uri, new RequestOptions() { PartitionKey = new PartitionKey(owner) });
            }
            catch (Exception ex)
            {

            }
            return collection;
        }

        public Collection Get(string id)
        {
            Collection collection = null;
            try
            {
                var uri = GetCollections(x => x.Id.Equals(new Guid(id)) && x.IsDeleted == false);

                collection = uri.FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return collection;
        }

        public async Task<bool> CreateCollection(BaseCollection collection)
        {
            bool success = false;
            try
            {
                var uri = UriFactory.CreateDocumentCollectionUri(_parameters.CollectionsDBDatabaseName, _parameters.CollectionsDBCollectionName);
                var response = await _client.CreateDocumentAsync(uri, collection);

                success = response.StatusCode == System.Net.HttpStatusCode.Created;
            }
            catch (Exception)
            {
                
            }
            
            return success;
        }

        public ICollection<Collection> GetSharedCollections(string owner)
        {
            return GetCollections(x => x.SharedWith.Contains(owner) && x.IsDeleted == false);
        }

        public ICollection<Collection> GetMyCollections(string owner)
        {
            return GetCollections(x => x.Owner.Equals(owner) && x.IsDeleted == false);
        }

        private ICollection<Collection> GetCollections(Func<Collection, bool> filter)
        {
            IEnumerable<Collection> collections = new List<Collection>();
            try
            {
                var uri = UriFactory.CreateDocumentCollectionUri(_parameters.CollectionsDBDatabaseName, _parameters.CollectionsDBCollectionName);

                collections = _client.CreateDocumentQuery<Collection>(uri)
                                     .Where(filter)
                                     .AsEnumerable();
            }
            catch (Exception ex)
            {

            }

            return collections.ToList();
        }

        public async Task<bool> Update(Collection collection)
        {
            bool success = false;
            try
            {
                var uri = UriFactory.CreateDocumentUri(_parameters.CollectionsDBDatabaseName, _parameters.CollectionsDBCollectionName, collection.Id.ToString());
                var response = await _client.ReplaceDocumentAsync(uri, collection);

                success = response.StatusCode == System.Net.HttpStatusCode.OK;
            }
            catch (Exception)
            {

            }

            return success;
        }
    }
}
