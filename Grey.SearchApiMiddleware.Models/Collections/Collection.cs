﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Models
{
    public class Collection : BaseCollection
    {
        public Collection()
        {
            Items = new List<dynamic>();
        }

        public List<dynamic> Items { get; set; }
    }
}
