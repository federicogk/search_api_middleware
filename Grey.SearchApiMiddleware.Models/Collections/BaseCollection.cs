﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Models
{
    public class BaseCollection
    {
        public BaseCollection()
        {
            ItemIds = new List<string>();
            SharedWith = new List<string>();
        }
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("application")]
        public string Application { get; set; }

        [JsonProperty("owner")]
        public string Owner { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("lastModifiedDate")]
        public DateTime LastModifiedDate { get; set; }

        [JsonProperty("sharedWith")]
        public ICollection<string> SharedWith { get; set; }

        [JsonProperty("itemIds")]
        public ICollection<string> ItemIds { get; set; }

        [JsonProperty("deletedDate")]
        public DateTime? DeletedDate { get; set; }

        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }



        public void Share(string userEmail)
        {
            if (!string.IsNullOrWhiteSpace(userEmail) && !IsSharedWithMe(userEmail))
            {
                SharedWith.Add(userEmail.ToLower());
                LastModifiedDate = DateTime.Now;
            }
        }

        public void Share(ICollection<string> userEmails)
        {
            if (userEmails != null)
            {
                foreach (var item in userEmails)
                {
                    Share(item);
                }
            }
        }

        public void Unshare(string userEmail)
        {
            if (!string.IsNullOrWhiteSpace(userEmail) && SharedWith.Contains(userEmail.ToLower()))
            {
                SharedWith.Remove(userEmail.ToLower());
                LastModifiedDate = DateTime.Now;
            }
        }

        public bool IsMine(string userEmail)
        {
            return !string.IsNullOrWhiteSpace(userEmail) && 
                   !string.IsNullOrWhiteSpace(Owner) &&
                   String.Equals(Owner.ToLower(), userEmail.ToLower(), StringComparison.InvariantCultureIgnoreCase);
        }

        public bool IsSharedWithMe(string username)
        {
            return !string.IsNullOrWhiteSpace(username) &&
                    SharedWith.Contains(username.ToLower());
        }

        public void AddItem(string itemId)
        {
            if (!string.IsNullOrWhiteSpace(itemId) && !ItemIds.Contains(itemId.ToLower()))
            {
                ItemIds.Add(itemId.ToLower());
                LastModifiedDate = DateTime.Now;
            }
        }

        public void RemoveItem(string itemId)
        {
            if (!string.IsNullOrWhiteSpace(itemId) && ItemIds.Contains(itemId.ToLower()))
            {
                ItemIds.Remove(itemId.ToLower());
                LastModifiedDate = DateTime.Now;
            }
        }

        public void Delete()
        {
            DeletedDate = DateTime.Now;
            LastModifiedDate = DeletedDate.Value;
            IsDeleted = true;
        }
        public void Recover()
        {
            DeletedDate = null;
            LastModifiedDate = DateTime.Now;
            IsDeleted = false;
        }

        public bool DoIHaveAccess(string user)
        {
            return (IsMine(user) || IsSharedWithMe(user));
        }

        public void ChangeName(string newName)
        {
            if (!string.IsNullOrWhiteSpace(newName))
            {
                Name = newName;
                LastModifiedDate = DateTime.Now;
            }
        }
    }
}
