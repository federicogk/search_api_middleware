﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Models
{
    public class BestOfItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Name { get; set; }

        [JsonProperty("thumbnail_url")]
        public string ThumbnailUrl { get; set; }

        [JsonProperty("video_url")]
        public string VideolUrl { get; set; }

        [JsonProperty("image_landscape_url")]
        public string ImageLandscapeUrl { get; set; }

        [JsonProperty("image_portrait_url")]
        public string ImagePortraitUrl { get; set; }

        [JsonProperty("image_mobile_url")]
        public string ImageMobileUrl { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("bestof_volume_id")]
        public string VolumeId { get; set; }

        [JsonProperty("bestof_volume_name")]
        public string VolumeName { get; set; }

        [JsonProperty("bestof_volume_desc")]
        public string VolumeDescription { get; set; }

        [JsonProperty("bestof_published_date")]
        public string PublishedDate { get; set; }
    }
}
