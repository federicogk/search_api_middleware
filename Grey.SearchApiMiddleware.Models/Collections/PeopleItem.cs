﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Models
{
    public class PeopleItem
    {
        public string Id { get; set; }

        public string Persona_Title { get; set; }

        public string Title { get; set; }

        [JsonProperty("thumbnail_url")]
        public string Thumbnail { get; set; }

        public ICollection<string> Agency { get; set; }

        public ICollection<string> Country { get; set; }
    }
}
