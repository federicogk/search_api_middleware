﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Models.BestOf
{
    public class PastIssue
    {
        [JsonProperty("bestof_volume_id")]
        public string VolumeId { get; set; }

        [JsonProperty("bestof_volume_name")]
        public string VolumeName { get; set; }

        [JsonProperty("bestof_volume_desc")]
        public string VolumeDesc { get; set; }

        [JsonProperty("bestof_archived_date")]
        public string ArchivedDate { get; set; }

        public DateTime GetArchivedDate()
        {
            try
            {
                return DateTime.Parse(ArchivedDate);
            }
            catch (Exception ex)
            {

            }
            return DateTime.Now;
        }
    }
}
