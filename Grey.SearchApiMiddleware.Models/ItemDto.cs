﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Models
{
    public class ItemDto
    {
        public ItemDto(string id, string name)
        {
            Id = id;
            Name = name;
        }
        public ItemDto(Guid id, string name)
        {
            Id = id.ToString();
            Name = name;
        }
        public ItemDto(int id, string name)
        {
            Id = id.ToString();
            Name = name;
        }

        public string Id { get; set; }

        public string Name { get; set; }
    }
}
