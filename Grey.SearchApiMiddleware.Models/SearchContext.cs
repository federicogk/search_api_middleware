﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Grey.SearchApiMiddleware.Models
{
    /// <summary>
    /// Represents the parameters that can be sent in the QueryString for search
    /// </summary>
    public class SearchContext
    {
        string _cacheAllPages = bool.FalseString;
        StringBuilder _sb;

        public SearchContext()
        {
            _sb = new StringBuilder();
        }

        /// <summary>
        /// Query: the query to search for. q=* means return all items.
        /// </summary>
        public string Q { get; set; }

        /// <summary>
        /// Facet: This is a comma separated list of facet_name:count. where: "facet_name" can be agency, region, client, brand, categories, source, discipline, media, channels, campaign, country, affiliated_people, isci_code, award_shows award_levels, city,department, business_region, skills, persona_title, locations, author.
        /// f = all  means all the facetable names above where: "count" is the top N number of facet to return, and "0" means all facets.
        /// </summary>
        public string F { get; set; }

        /// <summary>
        /// Filters the information by a specific Source.
        /// Source:   People = 'persona', ZonzaAssets = 'zonza', PublishAssets = 'publish', GreyImagesAssets = 'ggcc', AskGrey = 'askgrey', Connect = 'connect'
        /// </summary>
        public string S { get; set; }

        /// <summary>
        /// Filters. This parameter contains the filtered facets, in the format: A=filterName,filterValue1,filterValue2;otherFilterName,otherFilterValue1,otherFilterValue2
        /// </summary>
        public string A { get; set; }

        /// <summary>
        /// P = true means pretty format output
        /// </summary>
        public bool P { get; set; }

        /// <summary>
        /// M = any|all means matching any word or all words
        /// </summary>
        public string M { get; set; } //ANY | ALL

        /// <summary>
        /// O = fieldname asc|desc; it means to order the result by the fieldName in either ascending or descending order.
        /// </summary>
        public string O { get; set; }

        /// <summary>
        /// R = fieldname,fieldname,fielkdname,... It means to retrieve these comma separated list of fields.
        /// </summary>
        public string R { get; set; }

        /// <summary>
        /// D = fieldname,fieldname... It means to search these comma separated list of fields.
        /// </summary>
        public string D { get; set; }

        /// <summary>
        /// T = count. It means to return the top N results(max= 1000); default is 50.
        /// </summary>
        public int T { get; set; }

        /// <summary>
        /// K = count. It means to skip the first top K items; this is used to paginate results.
        /// </summary>
        public int K { get; set; }
        

        /// <summary>
        /// C = true or false ( t or f ). True means turn on spell Correction, default is true.
        /// </summary>
        public string C { get; set; }

        public string Page { get; set; }

        public string Client { get; set; }

        public string CacheAllPages
        {
            get
            {
                return _cacheAllPages.Equals("true", StringComparison.InvariantCultureIgnoreCase) ? bool.TrueString : bool.FalseString;
            }
            set
            {
                _cacheAllPages = value;
            }
        }

        /// <summary>
        /// Returns the key parameters that identifies the search.
        /// </summary>
        public Tuple<string, string, string, string> GetSearchKey()
        {
            string qKey = String.IsNullOrEmpty(Q) ? string.Empty : Q.ToLowerInvariant();
            string sKey = String.IsNullOrEmpty(S) ? string.Empty : S.ToLowerInvariant();
            string cKey = String.IsNullOrEmpty(C) ? string.Empty : C.ToLowerInvariant();
            string clientKey = String.IsNullOrEmpty(Client) ? string.Empty : Client.ToLowerInvariant();

            return new Tuple<string, string, string, string>(qKey, sKey, cKey, clientKey);
        }

        /// <summary>
        /// Returns the key parameters that identifies a suggestions request.
        /// </summary>
        public Tuple<string, string, string> GetSuggestionKey()
        {
            string qKey = String.IsNullOrEmpty(Q) ? string.Empty : Q.ToLowerInvariant();
            string sKey = String.IsNullOrEmpty(S) ? string.Empty : S.ToLowerInvariant();
            string cKey = String.IsNullOrEmpty(R) ? string.Empty : R.ToLowerInvariant();

            return new Tuple<string, string, string>(qKey, sKey, cKey);
        }

        public string GetQueryString()
        {
            _sb.Clear();

            if (!String.IsNullOrEmpty(Q)) { _sb.Append("&q=" + Q); }
            if (!String.IsNullOrEmpty(F)) { _sb.Append("&f=" + F); }
            if (!String.IsNullOrEmpty(S)) { _sb.Append("&s=" + S); }
            if (P) { _sb.Append("&p=" + P.ToString()); }
            if (!String.IsNullOrEmpty(M)) { _sb.Append("&m=" + M); }
            if (!String.IsNullOrEmpty(O)) { _sb.Append("&o=" + O); }
            if (!String.IsNullOrEmpty(R)) { _sb.Append("&r=" + R); }
            if (!String.IsNullOrEmpty(D)) { _sb.Append("&d=" + D); }
            if (T > 0) { _sb.Append("&t=" + T); }
            if (K > 0) { _sb.Append("&k=" + K); }
            if (!String.IsNullOrEmpty(C)) { _sb.Append("&c=" + C); }

            return String.IsNullOrEmpty(_sb.ToString()) ? "" : "?" + _sb.ToString().Substring(1);
        }

        public string GetQueryStringForAzureSearch()
        {
            _sb.Clear();

            if (!String.IsNullOrEmpty(Q)) { _sb.Append("&q=" + Q); }
            if (!String.IsNullOrEmpty(F)) { _sb.Append("&f=" + F); }
            if (!String.IsNullOrEmpty(S)) { _sb.Append("&s=" + S); }
            if (P) { _sb.Append("&p=" + P.ToString()); }
            if (!String.IsNullOrEmpty(M)) { _sb.Append("&m=" + M); }
            if (!String.IsNullOrEmpty(O)) { _sb.Append("&o=" + O); }
            if (!String.IsNullOrEmpty(R)) { _sb.Append("&r=" + R); }
            if (!String.IsNullOrEmpty(D)) { _sb.Append("&d=" + D); }
            
            if (!String.IsNullOrEmpty(C)) { _sb.Append("&c=" + C); }

            return String.IsNullOrEmpty(_sb.ToString()) ? "" : "?" + _sb.ToString().Substring(1);
        }

        public int GetPageNumber()
        {
            if (String.IsNullOrEmpty(Page) || !Int32.TryParse(Page, out int page))
            {
                page = 1;
            }

            return page;
        }

        public bool IsEmpty()
        {
            return String.IsNullOrEmpty(Q) && String.IsNullOrEmpty(S) && String.IsNullOrEmpty(D) && String.IsNullOrEmpty(R);
        }

        public IList<string> GetFacets()
        {
            var facets = new List<string>();

            if (!String.IsNullOrEmpty(F))
            {
                var rawFacets = F.Split(",");
                for (int i = 0; i < rawFacets.Length; i++)
                {
                    if (rawFacets[i].Contains(":"))
                    {
                        string[] parts = rawFacets[i].Split(":");
                        facets.Add(string.Format("{0}, count:{1}", parts[0].Trim(), parts[1].Trim()));
                    }
                    else
                    {
                        facets.Add(string.Format("{0}, count:0", rawFacets[i].Trim()));
                    }
                }

            }
            
            return facets;
        }

        public IList<string> GetFieldsToRetrieve()
        {
            return String.IsNullOrEmpty(R) ? new List<string>() : R.Split(",").Select(x => x.Trim()).ToList();
        }

        public IList<string> GetFieldsToSearchInto()
        {
            return String.IsNullOrEmpty(D) ? new List<string>() : D.Split(",").Select(x => x.Trim()).ToList();
        }

        public IList<string> GetOrderingFields()
        {
            return String.IsNullOrEmpty(O) ? new List<string>() : O.Split(",").Select(x => x.Trim()).ToList();
        }
    }
}
