﻿using System.Linq;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.Models
{
    public class Filter
    {
        public Filter(string name, IEnumerable<FilterValue> values)
        {
            Name = name;
            Values = values;
        }

        public string Name { get; set; }

        public IEnumerable<FilterValue> Values { get; set; }

        public IEnumerable<FilterValue> GetValuesByName()
        {
            return Values.OrderBy(x => x.Name);
        }

        public string CapitalizedName()
        {
            if (Name == null)
                return null;

            if (Name.Length > 1)
                return char.ToUpper(Name[0]) + Name.Substring(1);

            return Name.ToUpper();
        }
    }
}
