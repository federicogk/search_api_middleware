﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Models
{
    public class UserBasicInfo
    {
        [JsonProperty("title")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("thumbnail_url")]
        public string ThumbnailUrl { get; set; }

        [JsonProperty("thumbnail_url_cdn")]
        public string ThumbnailUrlCdn { get; set; }

        [JsonProperty("persona_title")]
        public string JobTitle { get; set; }

        [JsonProperty("id")]
        public string ECCode { get; set; }
    }
}
