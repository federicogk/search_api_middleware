﻿namespace Grey.SearchApiMiddleware.Models
{
    public class FilterValue
    {
        public FilterValue(string name, long count)
        {
            Name = name;
            Count = count;
        }

        public string Name { get; set; }

        public long Count { get; set; }
    }
}