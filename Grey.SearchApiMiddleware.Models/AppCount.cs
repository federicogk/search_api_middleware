﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grey.SearchApiMiddleware.Models
{
    public class AppCount
    {
        public AppCount(string name)
        {
            Name = name;
            Count = 0;
        }

        public AppCount(string name, long count)
        {
            Name = name;
            Count = count;
        }

        public string Name { get; set; }

        public long Count { get; set; }
    }
}
