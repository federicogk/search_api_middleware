﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    public interface ICacheableSearchKey
    {
        string Q { get; }

        string S { get; }

        string C { get; }
    }
}
