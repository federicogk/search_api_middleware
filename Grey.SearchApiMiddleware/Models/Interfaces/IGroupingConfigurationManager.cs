﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    public interface IGroupingConfigurationManager
    {
        GroupingConfiguration GetForSource(SearchItem item);
    }
}
