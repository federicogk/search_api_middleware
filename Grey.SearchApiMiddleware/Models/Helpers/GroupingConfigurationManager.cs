﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    public class GroupingConfigurationManager : IGroupingConfigurationManager
    {
        ICollection<GroupingConfiguration> _configurations;
        GroupingConfiguration _nullGroupingConfiguration;

        public GroupingConfigurationManager(IApiParameters parameters)
        {
            _configurations = parameters.GetGroupingConfiguration();
            _nullGroupingConfiguration = new GroupingConfiguration() { Name = "Unknown", Grouping = false, GroupCount = 100 };
        }

        public GroupingConfiguration GetForSource(SearchItem item)
        {
            GroupingConfiguration config = _configurations.FirstOrDefault(x => x.HasSource(item.source));

            return (config == null) ? _nullGroupingConfiguration : config;
        }
    }
}
