﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    public class GroupsContainer
    {
        ICollection<SearchGroup> _groups;

        public GroupsContainer()
        {
            _groups = new List<SearchGroup>();
        }

        public bool ExistsGroup(string key)
        {
            return Get(key) != null;
        }

        public void CreateGroup(string groupName)
        {
            SearchGroup group = new SearchGroup(groupName);
            _groups.Add(group);
        }

        public void CreateGroup(string groupName, SearchItem item)
        {
            SearchGroup group = new SearchGroup(groupName);
            group.Items.Add(item);
            _groups.Add(group);
        }

        public int GetItemsCount(string groupName)
        {
            return ExistsGroup(groupName) ? Get(groupName).Items.Count : 0;
        }

        public void AddToGroup(string groupName, SearchItem item)
        {
            SearchGroup group = Get(groupName);

            if (group != null)
            {
                group.Items.Add(item);
            }
        }

        public ICollection<SearchGroup> GetAllGroups()
        {
            return _groups;
        }

        private SearchGroup Get(string key)
        {
            return _groups.FirstOrDefault(x => x.Name.Equals(key, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
