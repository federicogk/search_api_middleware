﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    public class GroupingConfiguration
    {
        public GroupingConfiguration()
        {
            Sources = new List<SourceElement>();
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sources")]
        public ICollection<SourceElement> Sources { get; set; }

        [JsonProperty("grouping")]
        public bool Grouping { get; set; }

        [JsonProperty("groupCount")]
        public int GroupCount { get; set; }

        public bool HasSource(string sourceName)
        {
            return Sources.Any(x => x.Source.Equals(sourceName, StringComparison.InvariantCultureIgnoreCase));
        }
    }

    public class SourceElement
    {
        [JsonProperty("source")]
        public string Source { get; set; }
    }
}
