﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Grey.SearchApiMiddleware.Models
{
    public abstract class BaseResultPage
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("elapsedTime")]
        public string ElapsedTime { get; set; }

        [JsonProperty("@search.facets")]
        public JToken Facets { get; set; }

        [JsonProperty("suggestion")]
        public string Suggestion { get; set; }

        [JsonProperty("@odata.context")]
        public string ODataContext { get; set; }

        [JsonProperty("@odata.count")]
        public string ODataCount { get; set; }
    }
}
