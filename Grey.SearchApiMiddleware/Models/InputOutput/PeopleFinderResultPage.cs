﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.Models
{
    public class PeopleFinderResultPage : BaseResultPage
    {
        [JsonProperty("value")]
        public ICollection<SearchItem> Value { get; set; }
    }
}
