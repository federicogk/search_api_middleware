﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    public class PaginationInfo
    {
        public PaginationInfo(int total, int current)
        {
            TotalPages = total;
            CurrentPage = current;
        }

        [JsonProperty("totalPages")]
        public int TotalPages { get; set; }

        [JsonProperty("currentPage")]
        public int CurrentPage { get; set; }
    }
}
