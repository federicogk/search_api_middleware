﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.Models
{
    public class GPSearchResultPage : BaseResultPage
    {
        public GPSearchResultPage()
        {
            Pagination = new PaginationInfo(1, 1);
            Value = new List<SearchGroup>();
        }

        [JsonProperty("topResult")]
        public SearchItem TopResult { get; set; }

        [JsonProperty("pagination")]
        public PaginationInfo Pagination { get; set; }

        [JsonProperty("value")]
        public ICollection<SearchGroup> Value { get; set; }
    }
}
