﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    public class SearchResultPage
    {
        public SearchResultPage(int pageNumber, ICollection<SearchGroup> values)
        {
            PageNumber = pageNumber;
            Values = values;
        }

        public int PageNumber { get; set; }

        public ICollection<SearchGroup> Values { get; set; }
    }
}
