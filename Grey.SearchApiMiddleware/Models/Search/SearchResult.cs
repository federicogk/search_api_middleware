﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    /// <summary>
    /// Represents the full results of the search. This will be cached and retrieved for a specific page query.
    /// </summary>
    public class SearchResult
    {
        public SearchResult()
        {
            SearchResultItems = new List<SearchItem>();
        }

        public Metadata Metadata { get; set; }

        public SearchItem TopResult { get; set; }

        public ICollection<SearchItem> SearchResultItems { get; set; }
    }
}
