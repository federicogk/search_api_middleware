﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    public class Metadata
    {
        public string Url { get; set; }

        public string ElapsedTime { get; set; }

        public string Suggestion { get; set; }

        public string ODataContext { get; set; }

        public string ODataCount { get; set; }

        public JToken Facets { get; set; }
    }
}
