﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    /// <summary>
    /// Represents each search result item
    /// </summary>
    public class SearchItem
    {
        string _yearCreated;
        public SearchItem() {
            
        }

        [JsonProperty("@search.score")]
        public decimal SearchScore { get; set; }

        public decimal active { get; set; }

        public IEnumerable<string> affiliated_people { get; set; }

        public IEnumerable<string> affiliated_companies { get; set; }

        public IEnumerable<string> agency { get; set; }

        public IEnumerable<string> alternate_names { get; set; }

        public IEnumerable<string> author { get; set; }

        public IEnumerable<string> award_levels { get; set; }

        public IEnumerable<string> award_shows { get; set; }

        public IEnumerable<string> award_years { get; set; }

        public IEnumerable<string> award_brand { get; set; }

        public IEnumerable<string> business_region { get; set; }

        public IEnumerable<string> campaign { get; set; }

        public IEnumerable<string> categories { get; set; }

        public IEnumerable<string> channels { get; set; }

        public IEnumerable<string> city { get; set; }

        public IEnumerable<string> client { get; set; }

        public string content { get; set; }

        public string content_kw { get; set; }

        public IEnumerable<string> country { get; set; }

        public IEnumerable<string> department { get; set; }

        public string description { get; set; }

        public string description_kw { get; set; }

        public IEnumerable<string> discipline { get; set; }

        public string email { get; set; }

        public string file_type { get; set; }

        public string id { get; set; }

        public string isci_code { get; set; }

        public IEnumerable<string> locations { get; set; }

        public IEnumerable<string> media { get; set; }

        public string persona_title { get; set; }

        public string phone { get; set; }
        
        public IEnumerable<string> region { get; set; }

        public IEnumerable<string> skills { get; set; }

        public string source { get; set; }

        public string srcID { get; set; }

        public string src_Url { get; set; }

        public IEnumerable<string> tags { get; set; }

        public string thumbnail_url { get; set; }

        public string title { get; set; }

        public dynamic persona_favorites { get; set; }

        public string twitter { get; set; }

        public string linkedin { get; set; }

        public string facebook { get; set; }

        public string year_created
        {
            get; set;
            //get { return new DateTime(_yearCreated).ToString("0"); }
            //set { _yearCreated = value; }
        }
    }
}
