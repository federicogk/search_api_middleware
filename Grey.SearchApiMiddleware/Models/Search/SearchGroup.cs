﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Models
{
    /// <summary>
    /// Represents a group of Items grouped by a given configuration
    /// </summary>
    public class SearchGroup
    {
        public SearchGroup(string name)
        {
            Name = name;
            Items = new List<SearchItem>();
        }

        [JsonProperty("name")]
        public string Name { get; private set; }

        [JsonProperty("content")]
        public ICollection<SearchItem> Items { get; private set; }
    }
}
