﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyCompany("Grey")]
[assembly: AssemblyProduct("Grey.GPServices")]
[assembly: AssemblyDescription("Release Sprint 93")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Hotfix
//      Revision
//
[assembly: AssemblyVersion("1.2.0.0")]
[assembly: AssemblyFileVersion("1.2.0.0")]
