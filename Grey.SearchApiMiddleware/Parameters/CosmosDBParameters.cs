﻿using Grey.SearchApiMiddleware.AzureData;

namespace Grey.SearchApiMiddleware
{
    public class CosmosDBParameters : ICosmosDBParameters
    {
        public string CollectionsDBEndpoint { get; set; }
        public string CollectionsDBEndpointKey { get; set; }
        public string CollectionsDBDatabaseName { get; set; }
        public string CollectionsDBCollectionName { get; set; }
    }
}
