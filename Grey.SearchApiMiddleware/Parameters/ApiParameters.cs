﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grey.SearchApiMiddleware.Models;
using Newtonsoft.Json.Linq;

namespace Grey.SearchApiMiddleware
{
    public class ApiParameters : IApiParameters
    {
        public string AuthDiscoveryEndpoint { get; set; }
        public string AuthAllowedAudience { get; set; }
        public string AuthAllowedIssuer { get; set; }

        public string CosmosDBEndpoint { get; set; }
        public string CosmosDBEndpointKey { get; set; }
        public string CosmosDBDatabaseName { get; set; }
        public string CosmosDBCollectionName { get; set; }
        public string CosmosDBTokenTTL { get; set; }

        public string SearchPlatformUrl { get; set; }

        public string AzureSearchApiUrl { get; set; }
        public string AzureSearchApiVersion { get; set; }
        public string AzureSearchApiKey { get; set; }
        public string AzureFunctionSearchUrl { get; set; }

        public string SearchUrl { get; set; }
        public string SearchApiKey { get; set; }

        public string GroupsConfig { get; set; }
        public int MaxCnt { get; set; }
        public string IsDirectCalls { get; set; }

        public string SpellcheckUrl { get; set; }
        public string SpellcheckKey { get; set; }
        public string SpellcheckMode { get; set; }
        public string SpellcheckMkt { get; set; }
        public int ResultsCacheTTL { get; set; }

        public ICollection<GroupingConfiguration> GetGroupingConfiguration()
        {
            ICollection<GroupingConfiguration> groupingConfiguration = new List<GroupingConfiguration>();
            try
            {
                groupingConfiguration = Newtonsoft.Json.JsonConvert.DeserializeObject<ICollection<GroupingConfiguration>>(GroupsConfig);
            }
            catch (Exception ex)
            {

            }

            return groupingConfiguration;
        }

        public int GetMaxItemCountPerPage()
        {
            int total = 0;
            
            foreach (var item in GetGroupingConfiguration())
            {
                total += item.Grouping ? item.GroupCount : item.GroupCount * this.MaxCnt;
            }
            
            return total;
        }
    }
}
