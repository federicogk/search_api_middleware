﻿using Grey.SearchApiMiddleware.AzureSearch;

namespace Grey.SearchApiMiddleware
{
    public class AzureSearchServiceParameters : IAzureSearchServiceParameters
    {
        public string NewsIndexName { get; set; }
        public string GPIndexName { get; set; }
        public string ServiceKey { get; set; }
        public string ServiceName { get; set; }
    }
}
