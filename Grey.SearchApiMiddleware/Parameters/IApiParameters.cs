﻿using Grey.SearchApiMiddleware.Models;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware
{
    public interface IApiParameters
    {
        string AuthDiscoveryEndpoint { get; set; }
        string AuthAllowedAudience { get; set; }
        string AuthAllowedIssuer { get; set; }


        string CosmosDBEndpoint { get; set; }
        string CosmosDBEndpointKey { get; set; }
        string CosmosDBDatabaseName { get; set; }
        string CosmosDBCollectionName { get; set; }
        string CosmosDBTokenTTL { get; set; }

        string SearchPlatformUrl { get; set; }

        string AzureSearchApiUrl { get; set; }
        string AzureSearchApiVersion { get; set; }
        string AzureSearchApiKey { get; set; }
        string AzureFunctionSearchUrl { get; set; }

        string SearchUrl { get; set; }
        string SearchApiKey { get; set; }

        string GroupsConfig { get; set; }
        int MaxCnt { get; set; }
        string IsDirectCalls { get; set; }

        string SpellcheckUrl { get; set; }
        string SpellcheckKey { get; set; }
        string SpellcheckMode { get; set; }
        string SpellcheckMkt { get; set; }

        int ResultsCacheTTL { get; set; }

        ICollection<GroupingConfiguration> GetGroupingConfiguration();

        int GetMaxItemCountPerPage();
    }
}