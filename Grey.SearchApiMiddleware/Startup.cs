﻿using Grey.SearchApiMiddleware.AzureData;
using Grey.SearchApiMiddleware.Commands.AzureSearch;
using Grey.SearchApiMiddleware.Commands.BestOf;
using Grey.SearchApiMiddleware.Commands.Collections;
using Grey.SearchApiMiddleware.Commands.Users;
using Grey.SearchApiMiddleware.Models;
using Grey.SearchApiMiddleware.PostProcessors;
using Grey.SearchApiMiddleware.PreProcessors;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using c = Grey.SearchApiMiddleware.Commands;
using z = Grey.SearchApiMiddleware.AzureSearch;

namespace Grey.SearchApiMiddleware
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IApiParameters apiParameters = Configuration.GetSection("apiParameters").Get<ApiParameters>();
            z.IAzureSearchServiceParameters azureParameters = Configuration.GetSection("azureSearchServiceParameters").Get<AzureSearchServiceParameters>();
            ICosmosDBParameters cosmosDBParameters = Configuration.GetSection("cosmosDbParameters").Get<CosmosDBParameters>();

            //Services & Configs
            services.AddSingleton(apiParameters);
            services.AddSingleton(azureParameters);
            services.AddSingleton(cosmosDBParameters);
            services.AddSingleton<IDocumentClient>(new DocumentClient(new Uri(cosmosDBParameters.CollectionsDBEndpoint), cosmosDBParameters.CollectionsDBEndpointKey));

            services.AddSingleton<ICacheManager, CacheManager>();

            services.AddScoped<IGroupingConfigurationManager, GroupingConfigurationManager>();
            services.AddScoped<IGrouper, Grouper>();
            services.AddScoped<IPager, Pager>();
            services.AddScoped<IParser, Parser>();
            services.AddScoped<ITopResult, TopResult>();
            services.AddScoped<IOutputFormatter, OutputFormatter>();
            services.AddScoped<IResultBuilder, ResultBuilder>();

            //Commands
            services.AddScoped<IItemSourceChecker, ItemSourceChecker>();
            services.AddScoped<IGetFiltersCommand, GetFiltersCommand>();
            services.AddScoped<ICountPerAppCommand, CountPerAppCommand>();
            services.AddScoped<ICreateCollectionCommand, CreateCollectionCommand>();
            services.AddScoped<IGetCollectionsCommand, GetCollectionsCommand>();
            services.AddScoped<IGetCollectionsListCommand, GetCollectionsListCommand>();
            services.AddScoped<IAddItemToCollectionCommand, AddItemToCollectionCommand>();
            services.AddScoped<IShareCollectionCommand, ShareCollectionCommand>();
            services.AddScoped<IRemoveItemFromCollectionCommand, RemoveItemFromCollectionCommand>();
            services.AddScoped<IRemoveCollectionCommand, RemoveCollectionCommand>();
            services.AddScoped<IGetUserBasicInfoCommand, GetUserBasicInfoCommand>();
            services.AddScoped<IGetCollectionByIdCommand, GetCollectionByIdCommand>();
            services.AddScoped<IUpdateCollectionCommand, UpdateCollectionCommand>();

            services.AddScoped<IBestOfGetPastIssuesCommand, BestOfGetPastIssuesCommand>();

            //Azure Search
            services.AddScoped<z.IIndexClientProvider, z.IndexClientProvider>();
            services.AddScoped<z.IParametersBuilder, z.ParametersBuilder>();
            services.AddScoped<c.IAzureSearch, z.AzureSearch>();
            services.AddScoped<c.IIndexItemCounter, z.IndexItemCounter>();

            //Azure Data
            services.AddScoped<c.ICollectionsRepository, CollectionsRepository>();

            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //.AddJwtBearer(options =>
            //{
            //    options.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuer = true,
            //        ValidateAudience = true,
            //        ValidateLifetime = true,
            //        ValidateIssuerSigningKey = true,
            //        ValidIssuer = apiParameters.AuthAllowedIssuer,
            //        ValidAudience = apiParameters.AuthAllowedAudience 
            //    };
            //});

            //services.AddHttpClient();
            services.AddMemoryCache();
            services.AddCors();
            services.AddMvc();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            app.UseCors(x => x.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod().Build());

            //app.UseAuthentication();
            app.UseMvcWithDefaultRoute();
        }
    }
}
