﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.ViewModels
{
    public class UpdateCollectionViewModel
    {
        public string Name { get; set; }
    }
}
