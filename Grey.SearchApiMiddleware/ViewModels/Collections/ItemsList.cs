﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.ViewModels
{
    public class ItemsList
    {
        public ICollection<string> Items { get; set; }
    }
}
