﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.ViewModels
{
    public class CreateCollectionViewModel
    {
        public CreateCollectionViewModel()
        {
            Items = new List<string>();
            SharedWith = new List<string>();
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Application { get; set; }

        public ICollection<string> Items { get; set; }

        public ICollection<string> SharedWith { get; set; }
    }
}
