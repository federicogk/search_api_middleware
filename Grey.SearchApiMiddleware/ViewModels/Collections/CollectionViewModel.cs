﻿using System;
using System.Collections.Generic;
using model = Grey.SearchApiMiddleware.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.ViewModels
{
    public class CollectionViewModel
    {
        public CollectionViewModel()
        {

        }

        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public long Count { get; private set; }

        public string Application { get; private set; }

        public bool IsMine { get; private set; }
    
        public ICollection<dynamic> Items { get; private set; }

        public ICollection<string> SharedWith { get; private set; }

        public DateTime LastModifiedDate { get; private set; }

        public static CollectionViewModel From(model.Collection collection, string username)
        {
            var viewModel = new CollectionViewModel();
            viewModel.Id = collection.Id;
            viewModel.Name = collection.Name;
            viewModel.IsMine = collection.IsMine(username);
            viewModel.Application = collection.Application; 
            viewModel.LastModifiedDate = collection.LastModifiedDate;
            viewModel.Count = collection.Items.Count;
            viewModel.Items = collection.Items;
            viewModel.SharedWith = collection.SharedWith;
            return viewModel;
        }
    }
}
