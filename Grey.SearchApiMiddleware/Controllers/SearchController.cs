﻿using Grey.SearchApiMiddleware.Helpers;
using Grey.SearchApiMiddleware.Models;
using Grey.SearchApiMiddleware.PostProcessors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Controllers
{
    [Route("api/[controller]")]
    [DisableRequestSizeLimit]
    public class SearchController : Controller
    {
        IMemoryCache _cache;
        IApiParameters _parameters;
        IOutputFormatter _formatter;
        IResultBuilder _resultBuilder;
        ILogger<SearchController> _logger;
        IHttpClientFactory _httpClientFactory;

        public SearchController(IHttpClientFactory httpClientFactory, IMemoryCache cache, IApiParameters parameters, ILogger<SearchController> logger, IResultBuilder resultBuilder, IOutputFormatter formatter)
        {
            _cache = cache;
            _logger = logger;
            _formatter = formatter;
            _parameters = parameters;
            _resultBuilder = resultBuilder;
            _httpClientFactory = httpClientFactory;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromForm] SearchContext searchParameters, [FromHeader] string client)
        {
            searchParameters.Client = client;
            return await Search(searchParameters);
        }

        [HttpOptions]
        public async Task<ActionResult> Options([FromQuery] SearchContext searchParameters, [FromHeader] string client)
        {
            searchParameters.Client = client;
            return await Search(searchParameters);
        }

        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] SearchContext searchParameters, [FromHeader] string client)
        {
            searchParameters.Client = client;
            return await Search(searchParameters);
        }

        private async Task<ActionResult> Search(SearchContext searchParameters)
        {
            SearchResult searchResult = null;
            
            try
            {
                if (!_cache.TryGetValue(searchParameters.GetSearchKey(), out searchResult))
                {
                    string rawResults = await PerformAzureSearch(searchParameters);

                    searchResult = _resultBuilder.WithParameters(searchParameters).WithRawResults(rawResults).Build();

                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(_parameters.ResultsCacheTTL));

                    _cache.Set(searchParameters.GetSearchKey(), searchResult, cacheEntryOptions);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Search key {0}", searchParameters.GetSearchKey());
            }

            var output = _formatter.Format(searchResult, searchParameters);

            return Json(output);
        }

        private async Task<string> PerformAzureSearch(SearchContext searchParameters)
        {
            string results = string.Empty;
            try
            {
                var http = _httpClientFactory.CreateClient();

                var fullSearchUrl = String.Format("{0}{1}", _parameters.SearchUrl, searchParameters.GetQueryString());

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, fullSearchUrl);

                request.Headers.Add("Authorization", String.Format("Bearer {0}", _parameters.SearchApiKey));

                var searchRequest = await http.SendAsync(request);

                if (searchRequest.IsSuccessStatusCode)
                {
                    results = await searchRequest.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "PerformAzureSearch -> QueryString {0})", searchParameters.GetQueryStringForAzureSearch());
            }

            return results;
        }
    }
}
