﻿using Grey.SearchApiMiddleware.Commands.AzureSearch;
using Grey.SearchApiMiddleware.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Controllers
{
    [Produces("application/json")]
    [Route("api/Filters")]
    public class FiltersController : Controller
    {
        IMemoryCache _cache;
        IGetFiltersCommand GetFiltersCommand { get; set; }

        public FiltersController(IGetFiltersCommand getFiltersCommand, IMemoryCache cache)
        {
            _cache = cache;
            GetFiltersCommand = getFiltersCommand;
        }

        [HttpGet]
        public JsonResult Get([FromQuery] SearchContext searchParams, [FromHeader] string client)
        {
            IEnumerable<Filter> filters = new List<Filter>();

            if (!_cache.TryGetValue(string.Format("fil-{0}-{1}", client, searchParams.F), out filters))
            {
                GetFiltersCommand.Configure(searchParams).Execute();

                filters = GetFiltersCommand.Filters;

                var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(3000));

                _cache.Set(string.Format("fil-{0}-{1}", client, searchParams.F), filters, cacheEntryOptions);
            }

            return Json(filters);
        }
    }
}