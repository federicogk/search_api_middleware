﻿using Grey.SearchApiMiddleware.Commands.AzureSearch;
using Grey.SearchApiMiddleware.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Controllers
{
    [Produces("application/json")]
    [Route("api/CountPerApp")]
    [AllowAnonymous]
    public class CountPerAppController : Controller
    {
        ICountPerAppCommand CountPerAppCommand { get; set; }

        public CountPerAppController(ICountPerAppCommand command)
        {
            CountPerAppCommand = command;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            await CountPerAppCommand.Execute();

            return Json(CountPerAppCommand.Count);
        }
    }
}