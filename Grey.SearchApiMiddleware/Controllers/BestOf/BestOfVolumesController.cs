﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grey.SearchApiMiddleware.Commands.BestOf;
using Grey.SearchApiMiddleware.Models.BestOf;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Grey.SearchApiMiddleware.Controllers.BestOf
{
    [Produces("application/json")]
    [Route("api/BestOf/Volumes")]
    public class BestOfVolumesController : Controller
    {
        IBestOfGetPastIssuesCommand PastIssuesCommand { get; set; }

        public BestOfVolumesController(IBestOfGetPastIssuesCommand pastIssuesCommand)
        {
            PastIssuesCommand = pastIssuesCommand;
        }
        
        [HttpGet("Past")]
        public async Task<JsonResult> PastIssues()
        {
            await PastIssuesCommand.Execute();
            return Json(PastIssuesCommand.Issues.OrderByDescending(x => x.GetArchivedDate()));
        }
    }
}