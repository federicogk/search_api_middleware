﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grey.SearchApiMiddleware.Commands.AzureSearch;
using Grey.SearchApiMiddleware.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Grey.SearchApiMiddleware.Controllers.BestOf
{
    [Produces("application/json")]
    [Route("api/BestOf/Filters")]
    public class BestOfFiltersController : Controller
    {
        IMemoryCache _cache;
        IGetFiltersCommand GetFiltersCommand { get; set; }

        public BestOfFiltersController(IGetFiltersCommand getFiltersCommand, IMemoryCache cache)
        {
            _cache = cache;
            GetFiltersCommand = getFiltersCommand;
        }

        [HttpGet("{facets?}")]
        public async Task<JsonResult> Get(string facets)
        {
            IEnumerable<Filter> filters = new List<Filter>();

            if (string.IsNullOrEmpty(facets)) { facets = "agency,region,brand,industry"; }


            if (!_cache.TryGetValue($"fil-bestof-{facets}", out filters))
            {
                var context = new SearchContext() { Q = "*", S = "ggcc' and is_bestof eq '1", F = facets };

                await GetFiltersCommand.Configure(context).Execute();

                filters = GetFiltersCommand.Filters;

                var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(3000));

                _cache.Set($"fil-bestof-{facets}", filters, cacheEntryOptions);
            }

            return Json(filters.Select(x => new { Name = x.CapitalizedName(), Values = x.GetValuesByName() }));
        }
    }
}