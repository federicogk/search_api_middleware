﻿using Grey.SearchApiMiddleware.Commands.Collections;
using Grey.SearchApiMiddleware.Models;
using Grey.SearchApiMiddleware.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Controllers
{
    [Produces("application/json")]
    [Route("api/Collections")]
    //[Authorize]
    public class CollectionsController : Controller
    {
        IRemoveItemFromCollectionCommand RemoveItemFromCollectionCommand { get; set; }
        IAddItemToCollectionCommand AddItemToCollectionCommand { get; set; }
        IGetCollectionsListCommand GetCollectionsListCommand { get; set; }       
        IGetCollectionByIdCommand GetCollectionByIdCommand { get; set; }
        IRemoveCollectionCommand RemoveCollectionCommand { get; set; }
        IUpdateCollectionCommand UpdateCollectionCommand { get; set; }
        ICreateCollectionCommand CreateCollectionCommand { get; set; }
        IShareCollectionCommand ShareCollectionCommand { get; set; }
        IGetCollectionsCommand GetCollectionsCommand { get; set; }
        
        ICacheManager Cache { get; set; }

        public CollectionsController(ICreateCollectionCommand create, IGetCollectionsCommand get, IGetCollectionsListCommand list, 
                                     IAddItemToCollectionCommand add, IShareCollectionCommand share, IRemoveItemFromCollectionCommand remove,
                                     IRemoveCollectionCommand removeCollection, ICacheManager cache, 
                                     IGetCollectionByIdCommand getCollectionByIdCommand, IUpdateCollectionCommand update)
        {
            GetCollectionByIdCommand = getCollectionByIdCommand;
            RemoveCollectionCommand = removeCollection;
            RemoveItemFromCollectionCommand = remove;
            CreateCollectionCommand = create;
            GetCollectionsListCommand = list;
            AddItemToCollectionCommand = add;
            UpdateCollectionCommand = update;
            ShareCollectionCommand = share;
            GetCollectionsCommand = get;
            Cache = cache;
        }


        [HttpGet("List", Name = "CollectionsList")]
        public ActionResult GetMyCollectionsList()
        {
            string owner = GetCurrentUser();
            if (string.IsNullOrEmpty(owner))
            {
                return BadRequest("Unknown user");
            }
            else
            {
                ICollection<ItemDto> list = new List<ItemDto>();
                if (!Cache.TryGet("col-list-{0}", owner, out list))
                {
                    GetCollectionsListCommand.Configure(owner).Execute();
                    list = GetCollectionsListCommand.List;

                    Cache.Save("col-list-{0}", owner, list, 30);
                }
                return Json(list);
            }
        }

        [HttpGet("{collectionId}")]
        public async Task<ActionResult> GetCollection(string collectionId)
        {
            string owner = GetCurrentUser();
            if (string.IsNullOrEmpty(owner))
            {
                return BadRequest("Unknown user");
            }
            else
            {
                await GetCollectionByIdCommand.Configure(collectionId, owner).Execute();
                if (GetCollectionByIdCommand.Success)
                {
                    return Json(CollectionViewModel.From(GetCollectionByIdCommand.Collection, owner));
                }
                else
                {
                    return Json(GetCollectionByIdCommand.Message);
                }
            }
        }
        
        [HttpGet("Mine", Name = "MyCollections")]
        public async Task<ActionResult> GetMyCollections()
        {
            string owner = GetCurrentUser();
            if (string.IsNullOrEmpty(owner))
            {
                return BadRequest("Unknown user");
            }
            else
            {
                ICollection<Collection> myCollections = new List<Collection>();
                if (!Cache.TryGet("col-mine-{0}", owner, out myCollections))
                {
                    myCollections = await GetCollections(owner, "mine");
                }
                
                return Json(myCollections.OrderByDescending(x => x.LastModifiedDate).Select(x => CollectionViewModel.From(x, owner)));
            }
            
        }

        [HttpGet("SharedWithMe", Name = "CollectionsSharedWithMe")]
        public async Task<ActionResult> GetCollectionsSharedWithMe()
        {
            string owner = GetCurrentUser();
            if (string.IsNullOrEmpty(owner))
            {
                return BadRequest("Unknown user");
            }
            else
            {
                ICollection<Collection> sharedCollections = new List<Collection>();
                if (!Cache.TryGet("col-shared-{0}", owner, out sharedCollections))
                {
                    sharedCollections = await GetCollections(owner, "shared");
                }

                return Json(sharedCollections.OrderByDescending(x => x.LastModifiedDate).Select(x => CollectionViewModel.From(x, owner)));
            }
        }

        [HttpGet("All", Name = "AllCollections")]
        public async Task<ActionResult> GetAllCollections()
        {
            string owner = GetCurrentUser();
            if (string.IsNullOrEmpty(owner))
            {
                return BadRequest("Unknown user");
            }
            else
            {
                ICollection<Collection> allCollections = new List<Collection>();
                if (!Cache.TryGet("col-all-{0}", owner, out allCollections))
                {
                    allCollections = await GetCollections(owner, "all");
                }

                return Json(allCollections.OrderByDescending(x => x.LastModifiedDate).Select(x => CollectionViewModel.From(x, owner)));
            }            
        }


        [HttpPost]
        public async Task<ActionResult> CreateCollection([FromBody] CreateCollectionViewModel model)
        {
            string owner = GetCurrentUser();

            if (ModelState.IsValid && !string.IsNullOrEmpty(owner))
            {
                await CreateCollectionCommand.Configure(model.Name, model.Application, owner, model.Items, model.SharedWith).Execute();

                if (CreateCollectionCommand.Success)
                {
                    await RefreshCache(owner);
                    if (model.SharedWith != null && model.SharedWith.Count > 0)
                    {
                        foreach (var item in model.SharedWith)
                        {
                            await RefreshCache(item);
                        }
                    }
                }

                return Json(new { CreateCollectionCommand.Success, CreateCollectionCommand.Message });
            }
            else
            {
                var a = ModelState.Values.SelectMany(v => v.Errors);
                return BadRequest(a.Select(x => x.ErrorMessage));
            }
        }


        [HttpPatch("{collectionId}")]
        public async Task<ActionResult> ChangeName(string collectionId, [FromBody]UpdateCollectionViewModel newValues)
        {
            string owner = GetCurrentUser();
            if (String.IsNullOrEmpty(collectionId) || newValues == null || string.IsNullOrEmpty(newValues.Name))
            {
                return BadRequest();
            }
            else
            {
                await UpdateCollectionCommand.Configure(collectionId, owner, newValues.Name).Execute();

                if (UpdateCollectionCommand.Success) { await RefreshCache(owner); }

                return Json(new { UpdateCollectionCommand.Success, UpdateCollectionCommand.Message });
            }
        }

        [HttpPatch("{collectionId}/Add/{itemId}", Name = "AddItem")]
        public async Task<ActionResult> AddItem(string collectionId, string itemId)
        {
            string owner = GetCurrentUser();
            if (String.IsNullOrEmpty(collectionId) || string.IsNullOrEmpty(itemId) || string.IsNullOrEmpty(owner))
            {
                return BadRequest();
            }
            else
            {
                await AddItemToCollectionCommand.Configure(collectionId, owner, itemId).Execute();

                if (AddItemToCollectionCommand.Success) { await RefreshCache(owner); }

                return Json(new { AddItemToCollectionCommand.Success, AddItemToCollectionCommand.Message });
            }
        }

        [HttpPatch("{collectionId}/Add", Name = "AddItems")]
        public async Task<ActionResult> AddItems(string collectionId, [FromBody] ItemsList items)
        {
            string owner = GetCurrentUser();
            if (String.IsNullOrEmpty(collectionId) || items == null || string.IsNullOrEmpty(owner) || items.Items.Count == 0)
            {
                return BadRequest();
            }
            else
            {
                await AddItemToCollectionCommand.Configure(collectionId, owner, items.Items).Execute();

                if (AddItemToCollectionCommand.Success) { await RefreshCache(owner); }

                return Json(new { AddItemToCollectionCommand.Success, AddItemToCollectionCommand.Message });
            }
        }

        [HttpPatch("{collectionId}/Share", Name = "ShareCollection")]
        public async Task<ActionResult> ShareCollection(string collectionId, [FromBody] ItemsList users)
        {
            string owner = GetCurrentUser();
            if (string.IsNullOrEmpty(owner))
            {
                return BadRequest();
            }
            else
            {
                await ShareCollectionCommand.Configure(collectionId, users.Items).Execute();

                if (ShareCollectionCommand.Success)
                {
                    await RefreshCache(owner);
                    foreach (var item in users.Items)
                    {
                        await RefreshCache(item);
                    }
                }

                return Json(new { ShareCollectionCommand.Success, ShareCollectionCommand.Message });
            }
        }


        [HttpPatch("{collectionId}/Remove/{itemId}", Name = "RemoveItem")]
        public async Task<ActionResult> RemoveItem(string collectionId, string itemId)
        {
            string owner = GetCurrentUser();
            if (String.IsNullOrEmpty(collectionId) || string.IsNullOrEmpty(itemId) || string.IsNullOrEmpty(owner))
            {
                return BadRequest();
            }
            else
            {
                await RemoveItemFromCollectionCommand.Configure(collectionId, owner, itemId).Execute();

                if (RemoveItemFromCollectionCommand.Success) { await RefreshCache(owner); }

                return Json(new { RemoveItemFromCollectionCommand.Success, RemoveItemFromCollectionCommand.Message });
            }
        }

        [HttpPatch("{collectionId}/Remove", Name = "RemoveItems")]
        public async Task<ActionResult> RemoveItems(string collectionId, [FromBody] ItemsList items)
        {
            string owner = GetCurrentUser();
            if (String.IsNullOrEmpty(collectionId) || items == null || string.IsNullOrEmpty(owner) || items.Items.Count == 0)
            {
                return BadRequest();
            }
            else
            {
                await RemoveItemFromCollectionCommand.Configure(collectionId, owner, items.Items).Execute();

                if (RemoveItemFromCollectionCommand.Success) { await RefreshCache(owner); }

                return Json(new { RemoveItemFromCollectionCommand.Success, RemoveItemFromCollectionCommand.Message });
            }
        }


        [HttpDelete("{collectionId}")]
        public async Task<ActionResult> Delete(string collectionId)
        {
            string owner = GetCurrentUser();
            if (String.IsNullOrEmpty(collectionId) || string.IsNullOrEmpty(owner))
            {
                return BadRequest();
            }
            else
            {
                await RemoveCollectionCommand.Configure(collectionId, owner).Execute();

                if (RemoveCollectionCommand.Success) { await RefreshCache(owner); }

                return Json(new { RemoveCollectionCommand.Success, RemoveCollectionCommand.Message });
            }
        }


        private async Task RefreshCache(string owner)
        {
            Cache.Remove("col-mine-{0}", owner);
            Cache.Remove("col-shared-{0}", owner);
            Cache.Remove("col-all-{0}", owner);
            Cache.Remove("col-list-{0}", owner);

            await GetCollections(owner, "mine");
            await GetCollections(owner, "shared");
            await GetCollections(owner, "all");
        }

        private async Task<ICollection<Collection>> GetCollections(string user, string action)
        {
            switch (action)
            {
                case "mine":
                    await GetCollectionsCommand.Mine(user).Execute();
                    break;
                case "shared":
                    await GetCollectionsCommand.Shared(user).Execute();
                    break;
                case "all":
                    await GetCollectionsCommand.All(user).Execute();
                    break;
                default:
                    break;
            }
            Cache.Save("col-"+action+"-{0}", user, GetCollectionsCommand.Collections, 30);
            return GetCollectionsCommand.Collections;
        }

        protected virtual string GetCurrentUser()
        {
            string user = string.Empty;
            try
            {
                string hackUser = HttpContext.Request.Headers.Any(x => x.Key == "currentUser") ? HttpContext.Request.Headers["currentUser"].ToString() : string.Empty;
                user = HttpContext.User.Identity.Name == null ? hackUser : HttpContext.User.Identity.Name;
            }
            catch (Exception)
            {
            }
            return user;
        }
    }
}
