﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Grey.SearchApiMiddleware.Controllers
{
    [Produces("application/json")]
    [Route("api/Version")]
    public class VersionController : Controller
    {
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                string description = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
                string version = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyFileVersionAttribute>().Version;

                return Json($"Grey GPServices - Release: {description} - Version {version}");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}