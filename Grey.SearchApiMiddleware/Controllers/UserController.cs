﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grey.SearchApiMiddleware.Commands.Users;
using Grey.SearchApiMiddleware.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Grey.SearchApiMiddleware.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        IGetUserBasicInfoCommand GetBasicInfo { get; set; }
        ICacheManager Cache { get; set; }

        public UserController(ICacheManager cache, IGetUserBasicInfoCommand basic)
        {
            GetBasicInfo = basic;
            Cache = cache;
        }

        [HttpGet("Basic", Name = "BasicUserInfo")]
        public async Task<ActionResult> GetMyCollectionsList()
        {
            string currentUser = GetCurrentUser();
            if (string.IsNullOrEmpty(currentUser))
            {
                return BadRequest("Unknown user");
            }
            else
            {
                UserBasicInfo userBasicInfo = null;
                if (!Cache.TryGet("basic-info-{0}", currentUser, out userBasicInfo))
                {
                    await GetBasicInfo.Configure(currentUser).Execute();
                    userBasicInfo = GetBasicInfo.Info;

                    Cache.Save("basic-info-{0}", currentUser, userBasicInfo, 60);
                }

                return Json(userBasicInfo);
            }
        }

        protected virtual string GetCurrentUser()
        {
            string user = string.Empty;
            try
            {
                string hackUser = HttpContext.Request.Headers.Any(x => x.Key == "currentUser") ? HttpContext.Request.Headers["currentUser"].ToString() : string.Empty;
                user = HttpContext.User.Identity.Name == null ? hackUser : HttpContext.User.Identity.Name;
            }
            catch (Exception)
            {
            }
            return user;
        }
    }
}