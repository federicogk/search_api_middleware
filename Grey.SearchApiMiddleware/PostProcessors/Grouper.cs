﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public class Grouper : IGrouper
    {
        IGroupingConfigurationManager _groupingConfigurationManager;

        public Grouper(IGroupingConfigurationManager manager)
        {
            _groupingConfigurationManager = manager;
        }

        public ICollection<SearchGroup> GroupResults(ICollection<SearchItem> results)
        {
            GroupsContainer container = new GroupsContainer();

            foreach (var item in results)
            {
                var groupConfiguration = _groupingConfigurationManager.GetForSource(item);

                if (groupConfiguration.Grouping)
                {
                    if (!container.ExistsGroup(groupConfiguration.Name))
                    {
                        container.CreateGroup(groupConfiguration.Name);
                    }

                    if (container.GetItemsCount(groupConfiguration.Name) < groupConfiguration.GroupCount)
                    {
                        container.AddToGroup(groupConfiguration.Name, item);
                    }
                }
                else
                {
                    container.CreateGroup(groupConfiguration.Name, item);
                }
            }

            return container.GetAllGroups();
        }
    }
}
