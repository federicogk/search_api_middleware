﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public class TopResult : ITopResult
    {
        public SearchItem GetTopResult(SearchContext parameters, ICollection<SearchItem> items)
        {
            SearchItem topResult = null;
            
            for (int i = 0; i < items.Count; i++)
            {
                if (!String.IsNullOrEmpty(items.ElementAt(i).title))
                {
                    string titleClean = CleanString(items.ElementAt(i).title);
                    string queryClean = CleanString(parameters.Q);
                    if (titleClean == queryClean)
                    {
                        topResult = items.ElementAt(i);
                        break;
                    }
                }
            }

            return topResult;
        }

        private string CleanString(string stringToSanitize)
        {
            return Regex.Replace(stringToSanitize, @"[^a-zA-Z\d\s:]", "").Trim().ToLowerInvariant();
        }
    }
}
