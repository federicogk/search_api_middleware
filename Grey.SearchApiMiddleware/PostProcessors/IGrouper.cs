﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public interface IGrouper
    {
        ICollection<SearchGroup> GroupResults(ICollection<SearchItem> results);
    }
}
