﻿using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public class Pager : IPager
    {
        IApiParameters _parameters;
        public Pager(IApiParameters parameters)
        {
            _parameters = parameters;
        }

        public ICollection<SearchResultPage> PageGroups(ICollection<SearchGroup> groups)
        {
            ICollection<SearchResultPage> pages = new List<SearchResultPage>();

            int groupsPerPage = _parameters.MaxCnt > 0 ? _parameters.MaxCnt : 1;
            int numberOfPages = Decimal.ToInt32(Math.Ceiling((decimal)groups.Count / groupsPerPage));

            for (int i = 0; i < numberOfPages; i++)
            {
                var values = groups.Skip(groupsPerPage * i)
                                   .Take(groupsPerPage)
                                   .ToList();

                pages.Add(new SearchResultPage(i + 1, values));
            }

            return pages;
        }

        public ICollection<SearchItem> PageItems(ICollection<SearchItem> items, int skip)
        {
            ICollection<SearchResultPage> pages = new List<SearchResultPage>();

            int itermsPerPage = _parameters.MaxCnt > 0 ? _parameters.MaxCnt : 1;
            int numberOfPages = Decimal.ToInt32(Math.Ceiling((decimal)items.Count / itermsPerPage));

            return items.Skip(skip).Take(48).ToList();
        }
    }
}
