﻿using Grey.SearchApiMiddleware.Models;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public interface IPager
    {
        ICollection<SearchResultPage> PageGroups(ICollection<SearchGroup> groups);

        ICollection<SearchItem> PageItems(ICollection<SearchItem> items, int skip);
    }
}
