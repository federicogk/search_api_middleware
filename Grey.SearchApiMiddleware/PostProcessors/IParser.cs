﻿using Grey.SearchApiMiddleware.Models;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public interface IParser
    {
        void Load(string rawResults);

        ICollection<SearchItem> ParseSearchItems();

        Metadata ParseMetadata(SearchContext searchParameters);
    }
}
