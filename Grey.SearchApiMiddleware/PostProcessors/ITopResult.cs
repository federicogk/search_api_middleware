﻿using Grey.SearchApiMiddleware.Models;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public interface ITopResult
    {
        SearchItem GetTopResult(SearchContext parameters, ICollection<SearchItem> items);
    }
}
