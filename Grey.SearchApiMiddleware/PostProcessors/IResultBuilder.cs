﻿using Grey.SearchApiMiddleware.Models;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public interface IResultBuilder
    {
        IResultBuilder WithParameters(SearchContext searchParameters);

        IResultBuilder WithRawResults(string rawResults);

        SearchResult Build();
    }
}
