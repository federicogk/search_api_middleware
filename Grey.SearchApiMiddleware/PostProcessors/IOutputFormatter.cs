﻿using Grey.SearchApiMiddleware.Models;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public interface IOutputFormatter
    {
        dynamic Format(SearchResult results, SearchContext parameters);
    }
}
