﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grey.SearchApiMiddleware.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public class Parser : IParser
    {
        ILogger<Parser> _logger;
        JObject _data;
        string _raw;

        public Parser(ILogger<Parser> logger)
        {
            _logger = logger;
        }

        public void Load(string rawResults)
        {
            _raw = rawResults;
            _data = JObject.Parse(rawResults);
        }

        public Metadata ParseMetadata(SearchContext searchParameters)
        {
            Metadata metadata = new Metadata();
            try
            {
                if (_data != null)
                {
                    metadata.Url = _data["url"] == null ? string.Empty : _data["url"].ToString();
                    metadata.ElapsedTime = _data["elapseTime"] == null ? string.Empty : _data["elapseTime"].ToString();
                    metadata.Suggestion = _data["suggestion"] == null ? string.Empty : _data["suggestion"].ToString();
                    metadata.ODataContext = _data["@odata.context"] == null ? string.Empty : _data["@odata.context"].ToString();
                    metadata.ODataCount = _data["@odata.count"] == null ? string.Empty : _data["@odata.count"].ToString();
                    metadata.Facets = _data["@search.facets"] ?? new JObject("");
                }
                else
                {
                    metadata.Url = searchParameters.GetQueryString();
                    metadata.ElapsedTime = string.Empty;
                    metadata.Suggestion = string.Empty;
                    metadata.ODataContext = string.Empty;
                    metadata.ODataCount = string.Empty;
                    metadata.Facets = new JObject("");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "RawResults: {0}", _raw);
            }
            
            return metadata;
        }

        public ICollection<SearchItem> ParseSearchItems()
        {
            ICollection<SearchItem> items = new List<SearchItem>();

            try
            {
                if (_data["value"] != null)
                {
                    var values = _data["value"];

                    foreach (var item in values)
                    {
                        try
                        {
                            var searchItem = Newtonsoft.Json.JsonConvert.DeserializeObject<SearchItem>(item.ToString());
                            searchItem.SearchScore = item.Value<decimal>("@search.score");
                            items.Add(searchItem);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "Error extracting a specific Search Item. Value: {0}", item.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "General error extracting Search Items.");
            }

            return items;
        }
    }
}
