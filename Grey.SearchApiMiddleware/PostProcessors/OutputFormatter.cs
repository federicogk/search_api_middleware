﻿using Grey.SearchApiMiddleware.Helpers;
using Grey.SearchApiMiddleware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public class OutputFormatter : IOutputFormatter
    {
        IGrouper _grouper;
        IPager _pager;

        public OutputFormatter(IPager pager, IGrouper grouper)
        {
            _pager = pager;
            _grouper = grouper;
        }


        public dynamic Format(SearchResult results, SearchContext parameters)
        {
            if (parameters.Client == ClientApps.GP_SEARCH)
            {
                return ForGPSearch(results, parameters.GetPageNumber());
            }
            else
            {
                return ForPeopleFinder(results, parameters.K);
            }
        }

        private GPSearchResultPage ForGPSearch(SearchResult results, int page)
        {

            GPSearchResultPage output = new GPSearchResultPage();

            output.Url = results.Metadata.Url;
            output.ElapsedTime = results.Metadata.ElapsedTime;
            output.Suggestion = results.Metadata.Suggestion;
            output.ODataContext = results.Metadata.ODataContext;
            output.ODataCount = results.Metadata.ODataCount;
            output.Facets = results.Metadata.Facets;

            output.TopResult = results.TopResult;
            if (results.TopResult != null) { results.SearchResultItems.Remove(results.TopResult); }

            var groupedResults = _grouper.GroupResults(results.SearchResultItems);
            var pages = _pager.PageGroups(groupedResults);


            if (pages.Any(x => x.PageNumber == page))
            {
                output.Value = pages.First(x => x.PageNumber == page).Values;

                output.Pagination = new PaginationInfo(pages.Count, page);
            }

            return output;
        }

        private PeopleFinderResultPage ForPeopleFinder(SearchResult results, int skipItemsCount)
        {
            PeopleFinderResultPage output = new PeopleFinderResultPage();

            output.Url = results.Metadata.Url;
            output.ElapsedTime = results.Metadata.ElapsedTime;
            output.Suggestion = results.Metadata.Suggestion;
            output.ODataContext = results.Metadata.ODataContext;
            output.ODataCount = results.Metadata.ODataCount;
            output.Facets = results.Metadata.Facets;

            
            output.Value = _pager.PageItems(results.SearchResultItems, skipItemsCount);
            return output;
        }
    }
}
