﻿using Grey.SearchApiMiddleware.Models;
using Microsoft.Extensions.Logging;
using System;

namespace Grey.SearchApiMiddleware.PostProcessors
{
    public class ResultBuilder : IResultBuilder
    {
        ILogger<ResultBuilder> _logger;
        SearchResult _resultBeingBuild;

        SearchContext _searchParameters;
        ITopResult _topResult;
        IGrouper _grouper;
        IParser _parser;
        IPager _pager;

        string _rawResults;
        
        public ResultBuilder(ILogger<ResultBuilder> logger, ITopResult topResult, IGrouper grouper, IPager pager, IParser parser)
        {
            _logger = logger;
            _grouper = grouper;
            _parser = parser;
            _pager = pager;
            _topResult = topResult;
        }

        public IResultBuilder WithParameters(SearchContext searchParameters)
        {
            _searchParameters = searchParameters;
            return this;
        }

        public IResultBuilder WithRawResults(string rawResults)
        {
            _rawResults = rawResults;
            return this;
        }

        public SearchResult Build()
        {
            _resultBeingBuild = new SearchResult();

            try
            {
                _parser.Load(_rawResults);

                var items = _parser.ParseSearchItems();
                var metadata = _parser.ParseMetadata(_searchParameters);

                SearchItem topResult = _topResult.GetTopResult(_searchParameters, items);

                _resultBeingBuild.SearchResultItems = items;
                _resultBeingBuild.Metadata = metadata;
                _resultBeingBuild.TopResult = topResult;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in the Result Builder.");
            }
            finally
            {
                _rawResults = string.Empty;
            }    

            return _resultBeingBuild;
        }
    }
}
