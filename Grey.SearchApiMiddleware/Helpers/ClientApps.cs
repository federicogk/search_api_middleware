﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.Helpers
{
    public class ClientApps
    {
        public const string GP_SEARCH = "GPSearch";
        public const string PEOPLE_FINDER = "PeopleFinder";
        public const string BESTOF = "BestOf";
        public const string NEWS = "News";
        public const string INTELLIGENCE = "Intelligence";
        public const string UNKNOWN = "Unknown";
    }
}
