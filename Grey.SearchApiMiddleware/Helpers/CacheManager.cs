﻿using Microsoft.Extensions.Caching.Memory;
using System;

namespace Grey.SearchApiMiddleware
{
    public class CacheManager : ICacheManager
    {
        IMemoryCache _cache;

        public CacheManager(IMemoryCache cache)
        {
            _cache = cache;
        }

        public void Save(string key, object objectToBeCached, int minutes = 1)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(minutes));
            _cache.Set(key, objectToBeCached, cacheEntryOptions);
        }

        public void Save(string pattern, string key, object objectToBeCached, int minutes = 1)
        {
            Save(string.Format(pattern, key), objectToBeCached, minutes);
        }

        public bool TryGet<T>(string key, out T cachedObject) where T : class
        {
            return _cache.TryGetValue(key, out cachedObject);
        }
        public bool TryGet<T>(string pattern, string key, out T cachedObject) where T : class
        {
            return TryGet(string.Format(pattern, key), out cachedObject);
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }

        public void Remove(string pattern, string key)
        {
            Remove(string.Format(pattern, key));
        }
    }
}
