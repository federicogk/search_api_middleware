﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware
{
    public interface ICacheManager
    {
        bool TryGet<T>(string key, out T cachedObject) where T : class;

        bool TryGet<T>(string pattern, string key, out T cachedObject) where T : class;

        void Save(string key, object objectToBeCached, int minutes);

        void Save(string pattern, string key, object objectToBeCached, int minutes = 1);

        void Remove(string key);

        void Remove(string pattern, string key);
    }
}
