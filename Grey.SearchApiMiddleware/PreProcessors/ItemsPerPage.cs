﻿using Grey.SearchApiMiddleware.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Grey.SearchApiMiddleware.PreProcessors
{
    public class ItemsPerPage
    {
        ILogger<ItemsPerPage> _logger;
        readonly RequestDelegate _next;
        readonly IApiParameters _parameters;

        public ItemsPerPage(RequestDelegate next, IApiParameters parameters, ILogger<ItemsPerPage> logger)
        {
            _next = next;
            _logger = logger;
            _parameters = parameters;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                if (httpContext.Request.QueryString.HasValue)
                {
                    StringValues app = "";
                    httpContext.Request.Query.TryGetValue("client", out app);

                    if (app.Equals(ClientApps.GP_SEARCH))
                    {
                        var qpe = new QueryParameterEditor(httpContext.Request.QueryString.Value);

                        qpe.RemoveQueryParam("t");

                        if (qpe["page"] == "1")
                        {
                            qpe.SetQueryParam("t", _parameters.GetMaxItemCountPerPage().ToString());
                        }
                        else
                        {
                            qpe.SetQueryParam("t", Int32.MaxValue.ToString());
                        }

                        httpContext.Request.QueryString = new QueryString(qpe.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error trying to set the amount of items per page", "");
            }
            finally
            {
                await _next.Invoke(httpContext);
            }
        }
    }
}