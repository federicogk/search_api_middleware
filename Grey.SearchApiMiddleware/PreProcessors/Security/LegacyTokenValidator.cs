﻿using Microsoft.AspNetCore.Http;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.PreProcessors
{
    public class LegacyTokenValidator : ITokenValidator
    {
        IDocumentClient _client;
        IApiParameters _parameters;
        ILogger<Authentication> _logger;
        public LegacyTokenValidator(IDocumentClient documentClient, IApiParameters parameters, ILogger<Authentication> logger)
        {
            _client = documentClient;
            _logger = logger;
            _parameters = parameters;
        }

        public async Task<bool> IsValid(HttpRequest request)
        {
            bool exists = false;
            string token = request.Headers["Authentication"];
            try
            {
                if (!String.IsNullOrEmpty(token))
                {
                    exists = await _client.CreateDocumentQuery(UriFactory.CreateDocumentCollectionUri(_parameters.CosmosDBDatabaseName, _parameters.CosmosDBCollectionName))
                                                                                    .Where(x => x.Id.Equals(token))
                                                                                    .ToAsyncEnumerable()
                                                                                    .Any();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error trying to check if the token {0} exists in the security database", token);
            }

            return exists;
        }
    }
}
