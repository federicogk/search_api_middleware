﻿using Grey.SearchApiMiddleware.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.Documents;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using System;
using System.Threading.Tasks;
using static Grey.SearchApiMiddleware.Helpers.ClientApps;

namespace Grey.SearchApiMiddleware.PreProcessors
{
    public class Authentication
    {
        IDocumentClient _client;
        IApiParameters _parameters;
        readonly RequestDelegate _next;
        ILogger<Authentication> _logger;

        public Authentication(RequestDelegate next, IDocumentClient documentClient, IApiParameters parameters, ILogger<Authentication> logger)
        {
            _next = next;
            _logger = logger;
            _parameters = parameters;
            _client = documentClient;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            ITokenValidator validator = GetTokenValidator(httpContext.Request);

            if (await validator.IsValid(httpContext.Request))
            {
                await _next.Invoke(httpContext);
            }
            else
            {
                httpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                await httpContext.Response.WriteAsync("Unauthorized request");

                return;
            }
        }

        private ITokenValidator GetTokenValidator(HttpRequest request)
        {
            StringValues app = "";
            ITokenValidator validator;
            request.Headers.TryGetValue("client", out app);

            switch (app)
            {
                case ClientApps.GP_SEARCH: validator = new LegacyTokenValidator(_client, _parameters, _logger);
                    break;
                case ClientApps.PEOPLE_FINDER: validator = new OAuthTokenValidator(_parameters, _logger);
                    break;
                case ClientApps.UNKNOWN:
                    validator = new OAuthTokenValidator(_parameters, _logger);
                    break;
                default:
                    validator = new NullObjectTokenValidator();
                    break;
            }
            return validator;
        }
    }
}
