﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.PreProcessors
{
    public interface ITokenValidator
    {
        Task<bool> IsValid(HttpRequest request);
    }
}
