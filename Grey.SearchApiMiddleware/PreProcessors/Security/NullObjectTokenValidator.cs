﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.PreProcessors
{
    public class NullObjectTokenValidator : ITokenValidator
    {
        public async Task<bool> IsValid(HttpRequest request)
        {
            return true;
        }
    }
}
