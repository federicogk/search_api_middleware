﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.PreProcessors
{
    public class OAuthTokenValidator : ITokenValidator
    {
        IApiParameters _parameters;
        ILogger<Authentication> _logger;
        static OpenIdConnectConfiguration _config;

        public OAuthTokenValidator(IApiParameters parameters, ILogger<Authentication> logger)
        {
            _logger = logger;
            _parameters = parameters;
        }

        public async Task<bool> IsValid(HttpRequest request)
        {
            bool isValid = false;
            string token = request.Headers["Authorization"];

            try
            {
                if (!String.IsNullOrEmpty(token))
                {
                    token = token.Replace("Bearer", "").Trim();

                    if (_config == null)
                    {
                        string stsDiscoveryEndpoint = _parameters.AuthDiscoveryEndpoint;

                        var configManager = new ConfigurationManager<OpenIdConnectConfiguration>(stsDiscoveryEndpoint, new OpenIdConnectConfigurationRetriever(), new HttpDocumentRetriever());

                        _config = await configManager.GetConfigurationAsync();
                    }
                    
                    TokenValidationParameters validationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = true,
                        ValidateIssuer = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKeys = _config.SigningKeys,
                        ValidAudience = _parameters.AuthAllowedAudience,
                        ValidIssuer = _parameters.AuthAllowedIssuer
                    };


                    JwtSecurityTokenHandler tokendHandler = new JwtSecurityTokenHandler();

                    SecurityToken jwt;

                    var result = tokendHandler.ValidateToken(token, validationParameters, out jwt);

                    isValid = result != null && result.Identity.IsAuthenticated;

                    request.HttpContext.User = result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, token);
                isValid = false;
            }
            return isValid;
        }
    }
}
