﻿using Grey.SearchApiMiddleware.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Grey.SearchApiMiddleware.PreProcessors
{
    public class ClientDetector
    {
        RequestDelegate _next;
        IApiParameters _parameters;
        ILogger<ClientDetector> _logger;

        public ClientDetector(RequestDelegate next, IApiParameters parameters, ILogger<ClientDetector> logger)
        {
            _next = next;
            _logger = logger;
            _parameters = parameters;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                httpContext.Request.Headers.Add("client", ClientApps.UNKNOWN);

                

                //if (String.IsNullOrEmpty(httpContext.Request.Headers["Authentication"]))
                //{
                //    httpContext.Request.Headers.Add("client", ClientApps.PEOPLE_FINDER);
                //}
                //else
                //{
                //    httpContext.Request.Headers.Add("client", ClientApps.GP_SEARCH);
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error trying to detect the client application", "");
            }
            finally
            {
                await _next.Invoke(httpContext);
            }
        }
    }
}
