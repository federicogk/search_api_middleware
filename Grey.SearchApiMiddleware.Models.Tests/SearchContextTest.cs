using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Collections.Generic;

namespace Grey.SearchApiMiddleware.Models.Tests
{
    [TestClass]
    public class SearchContextTest
    {
        [TestMethod]
        public void GetKey_WhenCalled_MustReturnATupleWithAllTheKeys()
        {
            //Arrange
            SearchContext contextParams = new SearchContext();
            contextParams.Q = "federico.gomez";
            contextParams.S = "persona";
            contextParams.C = "false";
            contextParams.Client = "GPSearch";

            //Act
            var key = contextParams.GetSearchKey();

            //Assert
            Assert.AreEqual(key.Item1, "federico.gomez");
            Assert.AreEqual(key.Item2, "persona");
            Assert.AreEqual(key.Item3, "false");
            Assert.AreEqual(key.Item4, "gpsearch");
        }

        [TestMethod]
        public void GetKey_WhenParameterIsNotPresent_MustReturnEmptryString()
        {
            //Arrange
            SearchContext contextParams = new SearchContext();
            contextParams.Q = "federico.gomez";
            contextParams.C = "false";

            //Act
            var key = contextParams.GetSearchKey();

            //Assert
            Assert.AreEqual(key.Item1, "federico.gomez");
            Assert.AreEqual(key.Item2, string.Empty);
            Assert.AreEqual(key.Item3, "false");
        }


        [TestMethod]
        public void GetQueryString_WithOneParameter_MustAddQuestionMark()
        {
            //Arrange
            SearchContext contextParams = new SearchContext();
            contextParams.Q = "federico.gomez";

            //Act
            string queryString = contextParams.GetQueryString();

            //Assert
            Assert.AreEqual(queryString, "?q=federico.gomez");
        }

        [TestMethod]
        public void GetQueryString_WithTwoParameter_MustAddQuestionMarkAndAmpersand()
        {
            //Arrange
            SearchContext contextParams = new SearchContext();
            contextParams.Q = "federico.gomez";
            contextParams.S = "persona";

            //Act
            string queryString = contextParams.GetQueryString();

            //Assert
            Assert.AreEqual(queryString, "?q=federico.gomez&s=persona");
        }

        [TestMethod]
        public void GetQueryString_WithParameter_MustAddHisValueIfTrue()
        {
            //Arrange
            SearchContext contextParams = new SearchContext();
            contextParams.Q = "federico.gomez";
            contextParams.S = "persona";
            contextParams.P = true;

            //Act
            string queryString = contextParams.GetQueryString();

            //Assert
            Assert.AreEqual(queryString, "?q=federico.gomez&s=persona&p=True");
        }

        [TestMethod]
        public void GetQueryString_WithAllParameters_MustReturnFullQueryStringSkippingPageAndCacheParams()
        {
            //Arrange
            SearchContext contextParams = new SearchContext();
            contextParams.Q = "federico.gomez";
            contextParams.F = "EMEA,LATAM";
            contextParams.S = "persona";
            contextParams.P = true;
            contextParams.M = "ALL";
            contextParams.O = "email";
            contextParams.R = "title,email,givenname";
            contextParams.D = "title,email";
            contextParams.T = 40;
            contextParams.K = 15;
            contextParams.C = "f";
            contextParams.Page = "2";
            contextParams.CacheAllPages = "true";

            //Act
            string queryString = contextParams.GetQueryString();

            //Assert
            Assert.IsTrue(queryString.StartsWith("?"));
            Assert.IsTrue(queryString.Contains("q=federico.gomez"));
            Assert.IsTrue(queryString.Contains("&f=EMEA,LATAM"));
            Assert.IsTrue(queryString.Contains("&s=persona"));
            Assert.IsTrue(queryString.Contains("&p=True"));
            Assert.IsTrue(queryString.Contains("&m=ALL"));
            Assert.IsTrue(queryString.Contains("&o=email"));
            Assert.IsTrue(queryString.Contains("&r=title,email,givenname"));
            Assert.IsTrue(queryString.Contains("&d=title,email"));
            Assert.IsTrue(queryString.Contains("&t=40"));
            Assert.IsTrue(queryString.Contains("&k=15"));

            Assert.IsTrue(queryString.Contains("&c=f"));
            Assert.IsFalse(queryString.Contains("&page"));
            Assert.IsFalse(queryString.Contains("&CacheAllPages"));
        }

        [TestMethod]
        public void GetFacets_WhenFacetParamIsEmpty_MustReturnAnEmptyList()
        {
            //Arrange
            SearchContext context = new SearchContext();

            //Act
            var facets = context.GetFacets();

            //Assert
            Assert.IsNotNull(facets);
            Assert.AreEqual(0, facets.Count);
        }

        [TestMethod]
        public void GetFacets_WhenOneFacetWithNoLimitIsRequested_MustReturnListWithOneElementAndCountZero()
        {
            //Arrange
            SearchContext context = new SearchContext();
            context.F = "business_region";

            //Act
            var facets = context.GetFacets();

            //Assert
            Assert.IsNotNull(facets);
            Assert.AreEqual(1, facets.Count);
            Assert.AreEqual("business_region, count:0", facets[0]);
        }

        [TestMethod]
        public void GetFacets_WhenTwoFacetsWithNoLimitAreRequested_MustReturnListWithTwoElementAndCountZero()
        {
            //Arrange
            SearchContext context = new SearchContext();
            context.F = "business_region, agency";

            //Act
            var facets = context.GetFacets();

            //Assert
            Assert.IsNotNull(facets);
            Assert.AreEqual(2, facets.Count);
            Assert.AreEqual("business_region, count:0", facets[0]);
            Assert.AreEqual("agency, count:0", facets[1]);
        }

        [TestMethod]
        public void GetFacets_WhenOneFacetWithLimitIsRequested_MustReturnListWithOneElementAndHisCount()
        {
            //Arrange
            SearchContext context = new SearchContext();
            context.F = "business_region:5";

            //Act
            var facets = context.GetFacets();

            //Assert
            Assert.IsNotNull(facets);
            Assert.AreEqual(1, facets.Count);
            Assert.AreEqual("business_region, count:5", facets[0]);
        }

        [TestMethod]
        public void GetFacets_WhenTwoFacetsAreRequestedAndOneHasLimit_MustReturnListWithTwoElementAndHisCounts()
        {
            //Arrange
            SearchContext context = new SearchContext();
            context.F = "business_region:5, agency";

            //Act
            var facets = context.GetFacets();

            //Assert
            Assert.IsNotNull(facets);
            Assert.AreEqual(2, facets.Count);
            Assert.AreEqual("business_region, count:5", facets[0]);
            Assert.AreEqual("agency, count:0", facets[1]);
        }


        [TestMethod]
        public void GetFieldsToRetrieve_WhenParameterIsEmpty_MustReturnAnEmptyList()
        {
            //Arrange
            SearchContext context = new SearchContext();

            //Act
            IList<string> fieldsToRetrieve = context.GetFieldsToRetrieve();

            //Assert
            Assert.IsNotNull(fieldsToRetrieve);
            Assert.AreEqual(0, fieldsToRetrieve.Count);
        }

        [TestMethod]
        public void GetFieldsToRetrieve_WhenParameterHasValues_MustReturnAListWithAllElements()
        {
            //Arrange
            SearchContext context = new SearchContext();
            context.R = "agency, client, title";

            //Act
            IList<string> fieldsToRetrieve = context.GetFieldsToRetrieve();

            //Assert
            Assert.AreEqual(3, fieldsToRetrieve.Count);
            Assert.AreEqual("agency", fieldsToRetrieve[0]);
            Assert.AreEqual("client", fieldsToRetrieve[1]);
            Assert.AreEqual("title", fieldsToRetrieve[2]);
        }
    }
}

