﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Grey.SearchApiMiddleware.Models.Tests
{
    [TestClass]
    public class CollectionTests
    {
        [TestMethod]
        public void Share_WhenCreated_MustInitializeEmptyCollections()
        {
            //Arrange

            //Act
            Collection collection = new Collection();

            //Assert
            Assert.IsNotNull(collection.SharedWith);
            Assert.IsNotNull(collection.ItemIds);
            Assert.IsNotNull(collection.Items);
            Assert.AreEqual(0, collection.SharedWith.Count);
            Assert.AreEqual(0, collection.ItemIds.Count);
            Assert.AreEqual(0, collection.Items.Count);
        }

        [TestMethod]
        public void Share_WhenAnEmailIsNotInTheList_MustAddIt()
        {
            //Arrange
            Collection collection = new Collection();

            //Act
            collection.Share("federico.gomez@globant.com");

            //Assert
            Assert.AreEqual(1, collection.SharedWith.Count);
            Assert.AreEqual("federico.gomez@globant.com", collection.SharedWith.First());
        }

        [TestMethod]
        public void Share_WhenAnEmailIsInTheList_MustNotDuplicateIt()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Share("federico.gomez@globant.com");

            //Act
            collection.Share("federico.gomez@globant.com");

            //Assert
            Assert.AreEqual(1, collection.SharedWith.Count(x => x == "federico.gomez@globant.com"));
        }

        [TestMethod]
        public void Share_WhenAnEmailIsNotInLowercase_MustTransformItToLowercase()
        {
            //Arrange
            Collection collection = new Collection();

            //Act
            collection.Share("Federico.GOMEZ@globant.com");

            //Assert
            Assert.AreEqual("federico.gomez@globant.com", collection.SharedWith.First());
        }

        [TestMethod]
        public void Share_WhenAnEmailIsNullOrEmpty_MustDoNothing()
        {
            //Arrange
            Collection collection = new Collection();
            string email = string.Empty;

            //Act
            collection.Share(email);

            //Assert
            Assert.AreEqual(0, collection.SharedWith.Count);
        }

        [TestMethod]
        public void Share_WhenAListIsSubmitted_MustAddAllTheEmails()
        {
            //Arrange
            Collection collection = new Collection();
            ICollection<string> emails = new List<string>();
            emails.Add("federico.gomez@globant.com");
            emails.Add("wilmer.beltran@globant.com");
            emails.Add("demian.ortiz@globant.com");

            //Act
            collection.Share(emails);

            //Assert
            Assert.AreEqual(3, collection.SharedWith.Count);
        }

        [TestMethod]
        public void Share_WhenAListIsNull_MustDoNothing()
        {
            //Arrange
            Collection collection = new Collection();
            ICollection<string> emails = null;

            //Act
            collection.Share(emails);

            //Assert
            Assert.AreEqual(0, collection.SharedWith.Count);
        }

        [TestMethod]
        public void Share_WhenAnEmailIsAdded_LastModifiedDateMustBeUpdated()
        {
            //Arrange
            Collection collection = new Collection();
            collection.LastModifiedDate = DateTime.MinValue;

            //Act
            collection.Share("federico.gomez@globant.com");

            //Assert
            Assert.AreNotEqual(DateTime.MinValue, collection.LastModifiedDate);
        }

        /*=================================*/
        [TestMethod]
        public void Unshare_WhenTheEmailIsOnTheList_MustRemoveIt()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Share("federico.gomez@globant.com");

            //Act
            collection.Unshare("federico.gomez@globant.com");

            //Assert
            Assert.AreEqual(0, collection.SharedWith.Count);
        }

        [TestMethod]
        public void Unshare_WhenTheEmailIsNotInLowercase_MustConvertAndRemoveIt()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Share("federico.gomez@globant.com");

            //Act
            collection.Unshare("FEDERICO.gomez@globant.com");

            //Assert
            Assert.AreEqual(0, collection.SharedWith.Count);
        }

        [TestMethod]
        public void Unshare_WhenAnEmailIsRemoved_LastModifiedDateMustBeUpdated()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Share("federico.gomez@globant.com");
            collection.LastModifiedDate = DateTime.MinValue;

            //Act
            collection.Unshare("federico.gomez@globant.com");

            //Assert
            Assert.AreNotEqual(DateTime.MinValue, collection.LastModifiedDate);
        }

        [TestMethod]
        public void Unshare_WhenAnEmailIsNullOrEmpty_MustDoNothing()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Share("federico.gomez@globant.com");

            //Act
            collection.Unshare(null);

            //Assert
            Assert.AreEqual(1, collection.SharedWith.Count);
        }


        /*=================================*/
        [TestMethod]
        public void IsMine_WhenSubmittedEmailIsEqualToOwner_MustReturnTrue()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "federico.gomez@globant.com";

            //Act
            bool isMine = collection.IsMine("federico.gomez@globant.com");

            //Assert
            Assert.IsTrue(isMine);
        }

        [TestMethod]
        public void IsMine_WhenSubmittedEmailIsNotLowercaseAndEqualToOwner_MustConvertAndReturnTrue()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "federico.gomez@globant.com";

            //Act
            bool isMine = collection.IsMine("FEDERICO.gomez@globant.com");

            //Assert
            Assert.IsTrue(isMine);
        }

        [TestMethod]
        public void IsMine_WhenOwnerIsNotLowercaseAndEqualToSubmittedEmail_MustConvertAndReturnTrue()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "FEDERICO.gomez@globant.com";

            //Act
            bool isMine = collection.IsMine("federico.gomez@globant.com");

            //Assert
            Assert.IsTrue(isMine);
        }

        [TestMethod]
        public void IsMine_WhenOwnerIsDifferentThanubmittedEmail_MustConvertAndReturnFalse()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "federico.gomez@globant.com";

            //Act
            bool isMine = collection.IsMine("wilmer.beltran@globant.com");

            //Assert
            Assert.IsFalse(isMine);
        }

        [TestMethod]
        public void IsMine_WhenSubmittedEmailIsEmpty_MustDoNothingReturnFalse()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "federico.gomez@globant.com";

            //Act
            bool isMine = collection.IsMine(string.Empty);

            //Assert
            Assert.IsFalse(isMine);
        }

        [TestMethod]
        public void IsMine_WhenSubmittedEmailIsNull_MustDoNothingReturnFalse()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "federico.gomez@globant.com";

            //Act
            bool isMine = collection.IsMine(null);

            //Assert
            Assert.IsFalse(isMine);
        }

        [TestMethod]
        public void IsMine_WhenOwnerEmailIsNull_MustDoNothingReturnFalse()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = null;

            //Act
            bool isMine = collection.IsMine("federico.gomez@globant.com");

            //Assert
            Assert.IsFalse(isMine);
        }


        /*=================================*/
        [TestMethod]
        public void IsSharedWithMe_WhenSubmittedEmailIsInTheList_MustReturnTrue()
        {
            //Arrange
            Collection collection = new Collection();
            collection.SharedWith.Add("federico.gomez@globant.com");

            //Act
            bool isSharedWithMe = collection.IsSharedWithMe("federico.gomez@globant.com");

            //Assert
            Assert.IsTrue(isSharedWithMe);
        }

        [TestMethod]
        public void IsSharedWithMe_WhenSubmittedEmailIsInNotTheList_MustReturnFalse()
        {
            //Arrange
            Collection collection = new Collection();
            collection.SharedWith.Add("wilmer.beltran@globant.com");

            //Act
            bool isSharedWithMe = collection.IsSharedWithMe("federico.gomez@globant.com");

            //Assert
            Assert.IsFalse(isSharedWithMe);
        }

        [TestMethod]
        public void IsSharedWithMe_WhenSubmittedEmailIsInNotInLowercaseAndInTheList_MustConvertAndReturnTrue()
        {
            //Arrange
            Collection collection = new Collection();
            collection.SharedWith.Add("federico.gomez@globant.com");

            //Act
            bool isSharedWithMe = collection.IsSharedWithMe("federico.GOMEZ@globant.com");

            //Assert
            Assert.IsTrue(isSharedWithMe);
        }

        [TestMethod]
        public void IsSharedWithMe_WhenSubmittedEmailIsNullOrEmpty_MustDoNothingAndReturnFalse()
        {
            //Arrange
            Collection collection = new Collection();
            collection.SharedWith.Add("federico.gomez@globant.com");

            //Act
            bool isSharedWithMe = collection.IsSharedWithMe(null);

            //Assert
            Assert.IsFalse(isSharedWithMe);
        }


        /*=================================*/
        [TestMethod]
        public void AddItem_WhenTheItemIsNotInTheList_MustAddIt()
        {
            //Arrange
            Collection collection = new Collection();

            //Act
            collection.AddItem("ggcc_0001");

            //Assert
            Assert.AreEqual(1, collection.ItemIds.Count);
            Assert.IsTrue(collection.ItemIds.Contains("ggcc_0001"));
        }

        [TestMethod]
        public void AddItem_WhenTheItemIsAlreadyInTheList_MustNotAddIt()
        {
            //Arrange
            Collection collection = new Collection();
            collection.AddItem("ggcc_0001");

            //Act
            collection.AddItem("ggcc_0001");

            //Assert
            Assert.AreEqual(1, collection.ItemIds.Count);
        }

        [TestMethod]
        public void AddItem_WhenItemIsNullOrEmpty_MustDoNothing()
        {
            //Arrange
            Collection collection = new Collection();
            collection.AddItem("ggcc_0001");

            //Act
            collection.AddItem(null);

            //Assert
            Assert.AreEqual(1, collection.ItemIds.Count);
        }

        [TestMethod]
        public void AddItem_WhenAnItemIsAdded_LastModifiedDateMustBeUpdated()
        {
            //Arrange
            Collection collection = new Collection();
            collection.LastModifiedDate = DateTime.MinValue;

            //Act
            collection.AddItem("ggcc_0001");

            //Assert
            Assert.AreNotEqual(DateTime.MinValue, collection.LastModifiedDate);
        }

        [TestMethod]
        public void AddItem_WhenItemExistsAndNewOneInUppercaseIsSubmitted_MustNotBeAdded()
        {
            //Arrange
            Collection collection = new Collection();
            collection.AddItem("ggcc_0001");

            //Act
            collection.AddItem("GGCC_0001");

            //Assert
            Assert.AreEqual(1, collection.ItemIds.Count);
        }

        /*=================================*/
        [TestMethod]
        public void RemoveItem_WhenTheItemIsInTheList_MustRemoveIt()
        {
            //Arrange
            Collection collection = new Collection();
            collection.AddItem("ggcc_0001");
            collection.AddItem("ggcc_0002");

            //Act
            collection.RemoveItem("ggcc_0001");
            
            //Assert
            Assert.AreEqual(1, collection.ItemIds.Count);
            Assert.IsFalse(collection.ItemIds.Contains("ggcc_0001"));
        }

        [TestMethod]
        public void RemoveItem_WhenTheItemIsNotInTheList_MustDoNothing()
        {
            //Arrange
            Collection collection = new Collection();
            collection.AddItem("ggcc_0001");

            //Act
            collection.RemoveItem("ggcc_0040");

            //Assert
            Assert.AreEqual(1, collection.ItemIds.Count);
        }

        [TestMethod]
        public void RemoveItem_WhenItemIsNullOrEmpty_MustDoNothing()
        {
            //Arrange
            Collection collection = new Collection();
            collection.AddItem("ggcc_0001");

            //Act
            collection.RemoveItem(null);

            //Assert
            Assert.AreEqual(1, collection.ItemIds.Count);
        }

        [TestMethod]
        public void RemoveItem_WhenAnItemIsRemoved_LastModifiedDateMustBeUpdated()
        {
            //Arrange
            Collection collection = new Collection();
            collection.AddItem("ggcc_0001");
            collection.AddItem("ggcc_0002");
            collection.LastModifiedDate = DateTime.MinValue;

            //Act
            collection.RemoveItem("ggcc_0001");

            //Assert
            Assert.AreNotEqual(DateTime.MinValue, collection.LastModifiedDate);
        }

        [TestMethod]
        public void RemoveItem_WhenItemInUppercaseIsSubmitted_MustConvertAndBeRemoved()
        {
            //Arrange
            Collection collection = new Collection();
            collection.AddItem("ggcc_0001");
            collection.AddItem("ggcc_0002");

            //Act
            collection.RemoveItem("GGCC_0001");

            //Assert
            Assert.AreEqual(1, collection.ItemIds.Count);
        }


        /*=================================*/
        [TestMethod]
        public void Delete_WhenCalled_MustSetDeletedFlagAndDates()
        {
            //Arrange
            Collection collection = new Collection();

            //Act
            collection.Delete();

            //Assert
            Assert.IsTrue(collection.IsDeleted);
            Assert.AreEqual(collection.LastModifiedDate, collection.DeletedDate);
        }

        [TestMethod]
        public void Recover_WhenCalled_MustSetDeletedFlagAndDates()
        {
            //Arrange
            Collection collection = new Collection();
            collection.IsDeleted = true;

            //Act
            collection.Recover();

            //Assert
            Assert.IsFalse(collection.IsDeleted);
            Assert.IsNull(collection.DeletedDate);
            Assert.IsNotNull(collection.LastModifiedDate);
        }

        /*=================================*/
        [TestMethod]
        public void DoIHaveAccess_WhenImOwner_MustReturnTrue()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "federico.gomez@globant.com";

            //Act
            bool haveAccess = collection.DoIHaveAccess("federico.gomez@globant.com");

            //Arrange
            Assert.IsTrue(haveAccess);
        }

        [TestMethod]
        public void DoIHaveAccess_WhenImOwnerButSubmittedInUppercase_MustConvertAndReturnTrue()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "federico.gomez@globant.com";

            //Act
            bool haveAccess = collection.DoIHaveAccess("FEDERICO.gomez@globant.com");

            //Arrange
            Assert.IsTrue(haveAccess);
        }

        [TestMethod]
        public void DoIHaveAccess_WhenSubmittedEmailIsNull_MustReturnFalse()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "federico.gomez@globant.com";

            //Act
            bool haveAccess = collection.DoIHaveAccess(null);

            //Arrange
            Assert.IsFalse(haveAccess);
        }

        [TestMethod]
        public void DoIHaveAccess_WhenIsSharedWithMe_MustReturnTrue()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "wilmer.beltran@globant.com";
            collection.Share("federico.gomez@globant.com");

            //Act
            bool haveAccess = collection.DoIHaveAccess("federico.gomez@globant.com");

            //Arrange
            Assert.IsTrue(haveAccess);
        }

        [TestMethod]
        public void DoIHaveAccess_WhenIsSharedWithMeButEmailISUppercase_MustConvertAndReturnTrue()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "wilmer.beltran@globant.com";
            collection.Share("federico.gomez@globant.com");

            //Act
            bool haveAccess = collection.DoIHaveAccess("federico.GOMEZ@globant.com");

            //Arrange
            Assert.IsTrue(haveAccess);
        }

        [TestMethod]
        public void DoIHaveAccess_WhenSubmittedEmailIsNotOwnerNorInSharedList_MustReturnFalse()
        {
            //Arrange
            Collection collection = new Collection();
            collection.Owner = "wilmer.beltran@globant.com";
            collection.Share("federico.gomez@globant.com");

            //Act
            bool haveAccess = collection.DoIHaveAccess("demian.ortiz@globant.com");

            //Arrange
            Assert.IsFalse(haveAccess);
        }

        [TestMethod]
        public void ChangeName_WhenCalled_MustUpdateTheNameAndLastModifiedDate()
        {
            //Arrange
            Collection collection = new Collection();
            collection.LastModifiedDate = DateTime.MinValue;
            collection.Name = "oldName";

            //Act
            collection.ChangeName("newName");

            //Arrange
            Assert.AreNotEqual("oldName", collection.Name);
            Assert.AreNotEqual(DateTime.MinValue, collection.LastModifiedDate);
            Assert.AreEqual("newName", collection.Name);
        }

        [TestMethod]
        public void ChangeName_WhenSubmittedNameIsNullOrEmpty_MustDoNothing()
        {
            //Arrange
            Collection collection = new Collection();
            collection.LastModifiedDate = DateTime.MinValue;
            collection.Name = "oldName";

            //Act
            collection.ChangeName(null);

            //Arrange
            Assert.AreEqual("oldName", collection.Name);
            Assert.AreEqual(DateTime.MinValue, collection.LastModifiedDate);
        }
    }
}
